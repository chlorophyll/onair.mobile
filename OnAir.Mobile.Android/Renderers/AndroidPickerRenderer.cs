﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Database;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using OnAir.Mobile.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Ecommunity.Mobile.V2ComboBoxView), typeof(AndroidPickerRenderer))]
namespace OnAir.Mobile.Droid
{
    public class AndroidPickerRenderer : Xamarin.Forms.Platform.Android.AppCompat.ViewRenderer<Ecommunity.Mobile.V2ComboBoxView, Spinner>
    {
        private Spinner _spinner;
        private PickerSpinnerAdapter _adapter;

        public AndroidPickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Ecommunity.Mobile.V2ComboBoxView> e)
        {
            base.OnElementChanged(e);

            if (e.NewElement != null)
            {
                if (Control == null)
                {
                    _spinner = new Spinner(Context, SpinnerMode.Dropdown);
                    _adapter = new PickerSpinnerAdapter(Context, e.NewElement);
                    _spinner.Adapter = _adapter;

                    _spinner.ItemSelected += _spinner_ItemSelected;

                    SetNativeControl(_spinner);
                }

                if (e.NewElement.ItemsSource != null)
                {
                    _adapter.SetItems(e.NewElement.ItemsSource);
                }

                if (e.NewElement.SelectedItem != null)
                {
                    _spinner.SetSelection(e.NewElement.ItemsSource.IndexOf(e.NewElement.SelectedItem));
                }
                else
                {
                    _spinner.SetSelection(e.NewElement.SelectedIndex);
                }
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == nameof(Element.ItemsSource))
            {
                _adapter.SetItems(Element.ItemsSource);
            }
            else if (e.PropertyName == nameof(Element.SelectedIndex))
            {
                _spinner.SetSelection(Element.SelectedIndex);
            }
            else if (e.PropertyName == nameof(Element.SelectedItem))
            {
                _spinner.SetSelection(Element.ItemsSource.IndexOf(Element.SelectedItem));
            }
        }

        private void _spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Element.SelectedIndex = e.Position;
            Element.SelectedItem = Element.ItemsSource[e.Position];
        }
    }

    public class PickerSpinnerAdapter : BaseAdapter, ISpinnerAdapter
    {
        public override int Count => _items.Count;

        private Context _context;
        private System.Collections.IList _items;
        private Xamarin.Forms.Picker _element;

        public PickerSpinnerAdapter(Context context, Xamarin.Forms.Picker element)
        {
            _context = context;
            _element = element;
            _items = new List<string>();
        }

        public void SetItems(System.Collections.IList items)
        {
            if (_items != null && _items is INotifyCollectionChanged)
            {
                (_items as INotifyCollectionChanged).CollectionChanged -= PickerSpinnerAdapter_CollectionChanged;
            }

            if (items == null)
            {
                _items = new List<string>();
            }
            else
            {
                _items = items;
            }

            if (items is INotifyCollectionChanged)
            {
                (items as INotifyCollectionChanged).CollectionChanged += PickerSpinnerAdapter_CollectionChanged;
            }

            NotifyDataSetChanged();
        }

        private void PickerSpinnerAdapter_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyDataSetChanged();
        }

        public override Android.Views.View GetView(int position, Android.Views.View convertView, ViewGroup parent)
        {
            var view = new TextView(_context);
            view.SetTextSize(Android.Util.ComplexUnitType.Dip, (float)_element.FontSize);
            view.SetTextColor(_element.TextColor.ToAndroid());

            var paddingH = (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, 10, _context.Resources.DisplayMetrics);
            var paddingV = (int)TypedValue.ApplyDimension(ComplexUnitType.Dip, 6, _context.Resources.DisplayMetrics);
            view.SetPadding(paddingH, paddingV, paddingH, paddingV);

            var item = _items[position];

            var text = string.Empty;

            if (_element.ItemDisplayBinding != null && _element.ItemDisplayBinding is Binding)
            {
                try
                {
                    var binding = _element.ItemDisplayBinding as Binding;
                    text = item.GetType().GetProperty(binding.Path).GetValue(item, null).ToString();
                }
                catch (Exception)
                {
                    text = item.ToString();
                }
            }
            else
            {
                text = item.ToString();
            }

            view.Text = text;


            return view;
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return null;
        }

        public override long GetItemId(int position)
        {
            return position;
        }
    }
}