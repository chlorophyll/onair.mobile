using System;
namespace OnAir.Mobile
{
	public static class Ids
	{
		public static class phone320568
		{
			public static class SignInScrollViewContainer
			{
				public static readonly Guid Id = new Guid("8427f8b6-7ee1-475f-bb5f-3bbd3642866b");
				public static readonly Guid ControlScrollId = new Guid("0a9fdc35-0a93-43f3-8373-0e214934df53");
			}
			public static class Home
			{
				public static readonly Guid Id = new Guid("78b5f302-ba16-400c-9145-240b8b8f3caf");
			}
			public static class HomeMenu
			{
				public static readonly Guid Id = new Guid("ca75dd2f-e239-4bf7-8887-d0c347a9e070");
				public static readonly Guid ControlBoxViewId = new Guid("e5c42a2c-e36b-4c88-a8b9-68d873adf0d1");
				public static readonly Guid ControlLogoImageId = new Guid("1969c7c5-d134-4d39-9007-df356722f05d");
				public static readonly Guid ControlVersionLabelId = new Guid("10613234-2af7-473a-aa37-88add66e4a64");
				public static readonly Guid ControlAccountImageId = new Guid("e148c7cf-c7b1-4b74-963f-f0c252b7227b");
				public static readonly Guid ControlAccountLabelId = new Guid("98e281dc-13e5-426b-b6fa-2fe5c673f27e");
				public static readonly Guid ControlFlecheImageId = new Guid("1655a4bb-ab1d-4599-9ace-2dd96b44f67d");
				public static readonly Guid ControlButtonAccountId = new Guid("3a9b94fe-51ee-49b0-b405-420a2715dbc8");
				public static readonly Guid ControlTraitsparationBoxViewId = new Guid("bcf04952-2c16-41c5-8dd2-a9a6c2b05c35");
				public static readonly Guid ControlWebsiteImageId = new Guid("7edf5037-f4be-409e-ba54-06026b043d8a");
				public static readonly Guid ControlWebsiteLabelId = new Guid("1513fc98-9550-4f71-b40d-08ad7cb166d1");
				public static readonly Guid ControlFleche2ImageId = new Guid("c1f567c4-f953-478f-8a93-4539a0bf21e0");
				public static readonly Guid ControlWebsiteButtonId = new Guid("f201334f-1f5e-4fbe-ba5d-46e813cc9f28");
				public static readonly Guid ControlTraitspration2BoxViewId = new Guid("f61ad679-0fde-4848-a566-ad67cbe4151d");
				public static readonly Guid ControlForumImageId = new Guid("29804cc5-d2e4-42a3-96ff-fab26a6b77fc");
				public static readonly Guid ControlForumLabelId = new Guid("0ce9f454-e101-480d-826c-74bdd9a8f59b");
				public static readonly Guid ControlFlche3ImageId = new Guid("d132ed7e-6e92-49e4-921c-cf32a190e6ee");
				public static readonly Guid ControlForumButtonId = new Guid("3f036d99-07e9-4d9f-949e-92c71d523c1e");
				public static readonly Guid ControlTraitdesparation3BoxViewId = new Guid("99e1dc6c-c835-40d2-ab14-934041809a2f");
				public static readonly Guid ControlWikiImageId = new Guid("23e9a826-35b9-45a6-9c97-3e31555ef1e1");
				public static readonly Guid ControlWikiLabelId = new Guid("c4d584af-f771-44d4-902d-2746e8e4638c");
				public static readonly Guid ControlFlche4ImageId = new Guid("3a2c8671-3aa7-44c4-a93f-86c858e9c8d8");
				public static readonly Guid ControlWikiButtonId = new Guid("cc771520-1a0a-4cce-a976-d67cdee6c30a");
				public static readonly Guid ControlTraitdesparation4BoxViewId = new Guid("3fbeee80-b199-4e7c-a191-288487076cce");
				public static readonly Guid ControlDiscordImageId = new Guid("37421985-939b-4a3b-bed2-7be6fd26894e");
				public static readonly Guid ControlDiscordLabelId = new Guid("eece603b-3bc9-429e-9799-1b650070de32");
				public static readonly Guid ControlFlche5ImageId = new Guid("75063ac1-6665-450f-add6-b60448e3441c");
				public static readonly Guid ControlDiscordButtonId = new Guid("f1bd1930-61f9-4f26-b918-5021dcd85101");
			}
			public static class OnAir
			{
				public static readonly Guid Id = new Guid("07f47741-f7f6-43d0-b0f0-47114b15e65e");
				public static readonly Guid ControlBGbleuImageId = new Guid("03b4f018-f82c-4e5a-b395-26c4071965e3");
				public static readonly Guid ControlLastnewsLabelId = new Guid("32ceb2c8-19e9-4c28-8e80-0f4174b715e5");
				public static readonly Guid ControlV2CarouselId = new Guid("193d8fe5-7fb1-4253-8608-edc1cac6a76d");
				public static readonly Guid ControlCarouselIndicatorId = new Guid("666b69ed-078b-4b68-99a9-1b84cbc905f2");
				public static readonly Guid ControlJobsImageId = new Guid("b8cc7177-873a-47ec-93bd-f7444fc54e6a");
				public static readonly Guid ControljobfondnoirBoxViewId = new Guid("c2137efa-39db-49c6-b305-208fa77e53fa");
				public static readonly Guid ControlJOBSLabelId = new Guid("267a5ad5-11a0-4888-bccd-a1cbd1deb619");
				public static readonly Guid ControlV2ClickBoxId = new Guid("c252ba1e-d21f-4f86-acec-a1e6a6d6e344");
				public static readonly Guid ControlNotificationsLabelId = new Guid("7ca44052-5015-4502-8f2d-9b89265494e2");
				public static readonly Guid ControlV2ListViewId = new Guid("f7ab355e-fbe5-4d77-be10-95ec5aaa7694");
				public static readonly Guid ControlLabelId = new Guid("6fec3898-6f31-4c27-823c-f06a6c9c524a");
				public static readonly Guid ControlButtonId = new Guid("60a6b097-91a7-4a2f-8609-1b376869984a");
				public static readonly Guid ControlREFRESHButtonId = new Guid("5588589a-8b5d-423c-83f0-dab70b0b1649");
				public static readonly Guid ControlButton1Id = new Guid("56ab8563-1c3a-4777-b499-9afda7bd277c");
			}
			public static class HomeNewsCell
			{
				public static readonly Guid Id = new Guid("e5bf8bef-f642-4820-b989-6fc396792e7b");
				public static readonly Guid ControlBoxViewId = new Guid("4915f8b2-6f8b-4c8b-b2e4-d8241f2ebf21");
				public static readonly Guid ControlBGImageId = new Guid("4c2f1c0c-56b1-4ba7-b463-fc56ede61860");
				public static readonly Guid ControlVignetteImageId = new Guid("df537c04-def8-48c1-a40c-f547e8f64c18");
				public static readonly Guid ControlbgblancBoxViewcloneId = new Guid("a2cf56a8-ae7a-44f3-b121-4ba65732eef3");
				public static readonly Guid ControlbgblancBoxViewId = new Guid("fd11f5af-9926-447a-ae3c-eaeed8a69ca5");
				public static readonly Guid ControlTitreLabelId = new Guid("59c8339f-ae31-4ada-982f-99acdb18dfbb");
				public static readonly Guid ControlDateLabelId = new Guid("e0bf541c-d0ad-4fd3-a32e-d0d763090bc1");
				public static readonly Guid ControlbgcatgorieBoxViewId = new Guid("cc7e46ea-491e-45f2-9f14-38a8c168e362");
				public static readonly Guid ControlcategorieLabelId = new Guid("45854d24-1216-40c0-bc2d-0d8c0ff7dee0");
			}
			public static class NewsDetail
			{
				public static readonly Guid Id = new Guid("787cc495-6afe-4a4d-8fda-b314f54bc292");
				public static readonly Guid ControlImageId = new Guid("4917c679-4069-465a-be55-013dc6164dfa");
				public static readonly Guid ControlImage1Id = new Guid("2a330049-ad69-43ed-b751-5969359b7358");
				public static readonly Guid ControlBoxViewId = new Guid("07bb123f-e732-4cf2-aace-dd766bcedd5b");
				public static readonly Guid ControlTitreLabelId = new Guid("e96c84c5-4b6a-47bf-85b2-b238c0670738");
				public static readonly Guid ControlDateLabelId = new Guid("af3db95e-df8c-485a-a2c6-bbc10161aa8e");
				public static readonly Guid ControlWebViewId = new Guid("ae078cb1-d266-41ce-a048-bcbb07f3434c");
			}
			public static class MyAccount
			{
				public static readonly Guid Id = new Guid("0c85cbe1-5006-4c5f-bfbf-fa026577a811");
				public static readonly Guid ControlBarreduhautImageId = new Guid("054b858b-3d2b-4709-8b22-57d0c4240436");
				public static readonly Guid ControlbgnoirBoxViewId = new Guid("d9e1816b-b74e-4de7-a9b0-095a97ffaaf6");
				public static readonly Guid ControlBGBLANCBoxViewId = new Guid("633caf73-3e8a-4db5-bac4-582f88a559f8");
				public static readonly Guid ControlActivityIndicatorId = new Guid("9eb68669-d07f-46d6-857f-b4948c8dab04");
				public static readonly Guid ControlCOMPANYNAMELabelcloneId = new Guid("b6b6d78d-9685-4881-9f71-8f030d864fd5");
				public static readonly Guid ControlLabelCompanNameId = new Guid("daf3acbd-a8cc-4502-ac61-7db499eb0e6d");
				public static readonly Guid ControlCOMPANYCODELabelId = new Guid("e1097c3d-6a41-4fbb-a75b-93d03caf4025");
				public static readonly Guid ControlLabelCompanyCodeId = new Guid("9b8be3ec-0ec6-45fc-9af6-5fb1c56862de");
				public static readonly Guid ControlUSERNAMELabelId = new Guid("573cfaaa-c90b-4ca9-9bc5-fcb4d4e592e8");
				public static readonly Guid ControlLabelUserNameId = new Guid("bc08d43d-1095-4de7-972d-86d367705855");
				public static readonly Guid ControlUSEREMAILLabelcloneId = new Guid("bcbdd581-c2fa-4b83-b1d3-8b253c84b584");
				public static readonly Guid ControlLabelUserEmailId = new Guid("e8499377-6295-4d7e-a582-897b80d3c795");
				public static readonly Guid ControlWORLDLabelId = new Guid("4ef4d3e8-fc05-4119-880b-dff6188930cc");
				public static readonly Guid ControlV2ComboBoxButtonsWorldId = new Guid("2fe4ad8d-a41d-409b-bd35-f96a96717d7d");
				public static readonly Guid ControlBoxViewId = new Guid("2db89c53-264e-4ff5-82a2-e37c5044f24c");
				public static readonly Guid ControlLogoutImageId = new Guid("3638fd90-b44f-450b-aa53-cec6c815ddb4");
				public static readonly Guid ControlLogoutLabelId = new Guid("ce9efba6-3c09-47b8-bbeb-d3525c527ca6");
				public static readonly Guid ControlButtonLogoutId = new Guid("80322075-ed86-4e39-8569-2c8d1f11d6e5");
			}
			public static class Missions
			{
				public static readonly Guid Id = new Guid("97f897fc-5f9f-4ebb-a28a-1b5e2674afb8");
				public static readonly Guid ControlImageId = new Guid("cdfcaa25-8017-4cf1-a33f-98f1403ceedc");
				public static readonly Guid ControlBGblancImageId = new Guid("3f6f015f-1fcf-48b2-a71f-01e60ffbd640");
				public static readonly Guid ControlComboBoxButtonsTabsId = new Guid("e88f9258-467d-4523-9fe9-104e4b956a52");
				public static readonly Guid ControlBGblancBoxViewId = new Guid("913bb4ee-eeee-4d3f-bf20-fe1c821489da");
				public static readonly Guid ControlEntrySearchICAOId = new Guid("7be91297-7094-4a46-9a71-454c2af45557");
				public static readonly Guid ControlButtonSearchId = new Guid("c7182add-2dc1-4dbe-82c7-9b1f9d3cfdfa");
				public static readonly Guid ControlBOUTONFILTREImageId = new Guid("304e184b-7433-4659-80ee-47f41b4aef97");
				public static readonly Guid ControlButtonExpandId = new Guid("8e83bd61-b3de-423b-986a-fe9aefd85492");
				public static readonly Guid ControlV2ListViewId = new Guid("db084db9-b086-41d9-8259-3b946d22a263");
				public static readonly Guid ControlUserControlSearchId = new Guid("bed2851c-ff42-4c1f-badb-ed40fbae34ab");
				public static readonly Guid ControlLoupeImageId = new Guid("1ad02274-3876-44f6-80fa-cd986c19fd02");
				public static readonly Guid ControlV2ComboBoxButtonsOrderId = new Guid("8f9cc6b2-1328-45d7-b070-cce266007cfb");
			}
			public static class MissionsCell
			{
				public static readonly Guid Id = new Guid("3e746846-afe8-4fc0-ba01-4bb8ea4b5b79");
				public static readonly Guid ControlBGBLANCBoxViewId = new Guid("b7b12a82-37ea-4824-94a7-cf2624343b2b");
				public static readonly Guid ControlGAUCHEBoxViewId = new Guid("d9d77831-0cf5-447c-b2a9-44c647ede67c");
				public static readonly Guid ControlMILIEUBoxViewId = new Guid("5c4aa6a1-5dfb-440a-b178-d431eb3e9c4f");
				public static readonly Guid ControlDROITEBoxViewId = new Guid("e8cc2695-6c46-44c5-aa9e-796aae9c2e33");
				public static readonly Guid ControlTITRELabelId = new Guid("1bf00f4d-2e07-4a5e-afc2-6b3d44ace2f5");
				public static readonly Guid ControlHEADINGImageId = new Guid("7639db70-fe12-45b0-a85c-aa5db84807cd");
				public static readonly Guid ControlHEADINGLabelId = new Guid("79ffd61e-58e9-49f0-83c3-4bb0f2aed87b");
				public static readonly Guid ControlDISTANCEImageId = new Guid("3fbe0f8a-3b83-49b1-b3da-bd0616c567f0");
				public static readonly Guid ControlDISTANCELabelId = new Guid("9ba60728-4038-437b-a8bd-0a6b71c78927");
				public static readonly Guid ControlCARGOImageId = new Guid("a5693f71-e117-49c0-b9d0-695a0f3616f1");
				public static readonly Guid ControlCARGOLabelId = new Guid("c704e8cd-8919-433f-80c5-9b8e27ba1f2b");
				public static readonly Guid ControlPAXImageId = new Guid("f9320f1d-c338-46b5-90cc-4081ac30b8ac");
				public static readonly Guid ControlPAXLabelId = new Guid("81d6130f-aa68-4b25-88f4-c82f00b1e878");
				public static readonly Guid ControlPENALTYImageId = new Guid("e26787d5-a98d-410d-a95d-9f0492c75dbc");
				public static readonly Guid ControlPENALTYLabelId = new Guid("6b1f891d-1421-4adb-ab43-3fef27002847");
				public static readonly Guid ControlPAYLabelId = new Guid("927b036e-0f26-4058-904b-ebc8db0a7eae");
				public static readonly Guid ControlPAYImageId = new Guid("a2603ed7-1d7b-49db-a9ee-2903870d441a");
				public static readonly Guid ControlCONSTRAINTEBYTIMEImageId = new Guid("41bf7469-0ff9-4356-ab5b-7478ae8ccdef");
				public static readonly Guid ControlEXPIRATIONDATEBoxViewId = new Guid("b37ffbc6-b2c7-4282-b237-1622eaabdacd");
				public static readonly Guid ControlEXPIRATIONDATELabelId = new Guid("e4303b67-cfc3-4613-9f51-8e871a8ceb9b");
				public static readonly Guid ControlBASELabelId = new Guid("7d488c93-63a9-45eb-996e-7d5b60f474a1");
				public static readonly Guid ControlICAOLabelId = new Guid("591a1218-8278-4741-ba0e-8aa40c9ab958");
				public static readonly Guid ControlEXPLabelId = new Guid("3c3e4716-1847-4535-9053-9f9c4103e4ea");
				public static readonly Guid ControlCOINPENALTYImageId = new Guid("0970e6ad-e3cc-4b49-84ff-23c09d726675");
				public static readonly Guid ControlCOINPAYImageId = new Guid("a231c2f0-7ebd-4841-baa6-9297648b8dd6");
				public static readonly Guid ControlICAOLabel1Id = new Guid("baba31fb-407f-4ca4-85df-3fade8214bd2");
				public static readonly Guid ControlImageId = new Guid("3397b8eb-e71b-4401-9fed-ce746c557362");
			}
			public static class MissionTabDetail
			{
				public static readonly Guid Id = new Guid("a1f759f7-1ba8-442f-b737-49fbaad5628b");
				public static readonly Guid ControlBoxViewNavigationBarId = new Guid("965cc96a-ee77-4adc-82e2-1cd0a1eeb199");
				public static readonly Guid ControlButtonTakeMissionId = new Guid("04a7fecd-f7e2-4055-aac1-a9b69b9c2c81");
				public static readonly Guid ControlActivityIndicatorId = new Guid("484054b4-ab30-4e9b-9e1f-5499774850b2");
				public static readonly Guid ControlFONDBLANCBoxViewId = new Guid("6d2a02c2-333a-4e27-96c2-1a679c7afa54");
				public static readonly Guid ControlGAUCHEBoxViewcloneId = new Guid("aacaf800-1d68-40ff-9c5f-73dbe6cfc474");
				public static readonly Guid ControlDROITEBoxViewcloneId = new Guid("85758c6b-3153-41ab-84cc-99107dabff61");
				public static readonly Guid ControlHEADINGImagecloneId = new Guid("410f311b-8541-4190-85b7-a0da6762029a");
				public static readonly Guid ControlHEADINGLabelcloneId = new Guid("9b224d21-59b7-4297-9a06-d25d7420244b");
				public static readonly Guid ControlDISTANCEImagecloneId = new Guid("f0516afd-c8c6-425d-a27e-4b4df41fc936");
				public static readonly Guid ControlDISTANCELabelcloneId = new Guid("f1f021fc-c041-4271-b79d-4133f2e6a36a");
				public static readonly Guid ControlMILIEUBoxViewcloneId = new Guid("bf22836d-6035-4602-8558-97f5d369f2dc");
				public static readonly Guid ControlTITRELabelcloneId = new Guid("edaafed0-f5ce-41e5-8ea4-7999e62afdce");
				public static readonly Guid ControlEXPIRATIONDATEBoxViewcloneId = new Guid("bd1038d6-aae5-44e0-aa30-daa4cae57547");
				public static readonly Guid ControlEXPIRATIONDATELabelcloneId = new Guid("145cd501-1e73-42af-8839-2ceb08c4b21f");
				public static readonly Guid ControlCARGOImagecloneId = new Guid("6123aa85-347e-4a83-91e1-36d3474912bb");
				public static readonly Guid ControlCARGOLabelcloneId = new Guid("ac6ac102-33f6-45cc-8e96-e64ccb5b1294");
				public static readonly Guid ControlPAXImagecloneId = new Guid("30135404-a640-4812-a63d-0a61a993ea9d");
				public static readonly Guid ControlPAXLabelcloneId = new Guid("a0072a2f-7147-4cb3-8dd4-5027778c03df");
				public static readonly Guid ControlXPLabelcloneId = new Guid("9d355359-deee-4cb5-8f90-e22f2a87fbd1");
				public static readonly Guid ControlXPRESULTATLabelcloneId = new Guid("b41d3241-74a0-4c70-9f39-db906d55c66c");
				public static readonly Guid ControlPENALTYImagecloneId = new Guid("e512bb1d-433f-44c3-9cca-525c810c39d6");
				public static readonly Guid ControlPENALTYLabelcloneId = new Guid("bbe760c8-b24d-4dd6-8c50-7505284e33c7");
				public static readonly Guid ControlPAYImagecloneId = new Guid("76e5f658-eeea-4f87-9576-051a79b6a0c0");
				public static readonly Guid ControlPAYLabelcloneId = new Guid("9b54d810-daef-43f7-b873-e0ef647699a4");
				public static readonly Guid ControlCONSTRAINTEBYTIMEImagecloneId = new Guid("754d3f70-4745-43b8-a749-25ba2ea43895");
				public static readonly Guid ControlV2ListViewcloneId = new Guid("fd5512a3-1333-4fa4-8791-7ce85aa93518");
				public static readonly Guid ControltriangleImagecloneId = new Guid("3701ee7a-c0cf-4c12-8588-febe69ddad02");
				public static readonly Guid ControlButtonInfosId = new Guid("ab5a45aa-9220-4c3d-b41a-f45f41f27594");
				public static readonly Guid ControlBASELabelId = new Guid("ac1e64a9-12a2-49ee-b57e-04891a25e3b8");
				public static readonly Guid ControlBASERESULTLabelId = new Guid("66ecf8e0-a0c6-487f-b504-aeb20ee965ed");
				public static readonly Guid ControlCOINPENALTYImagecloneId = new Guid("608298be-948b-4865-80e0-6c0baab8b264");
				public static readonly Guid ControlCOINPENALTYImageclonecloneId = new Guid("f9316177-e2d9-4d8a-8804-968b1d1ac277");
				public static readonly Guid ControlButtonnoentryId = new Guid("3e691cf7-9554-44b3-8040-2ad86cf93a6b");
				public static readonly Guid ControlV2ClickBoxId = new Guid("f00e4a1b-958b-4f67-b2f0-40d6073b6b6e");
			}
			public static class MissionTabs
			{
				public static readonly Guid Id = new Guid("ef54e241-bf45-4321-b174-a86c40674e1d");
			}
			public static class MissionsUCSearch
			{
				public static readonly Guid Id = new Guid("4ed5ec61-15ba-4a82-9958-d1b1d62757a2");
				public static readonly Guid ControlCONTOURNOIRBoxViewcloneId = new Guid("9b78284f-e4d7-47df-a84f-3e6f4ada1084");
				public static readonly Guid ControlBackgroundId = new Guid("1da72f43-6b69-4a81-82de-d67c85576671");
				public static readonly Guid ControlV2ComboBoxRangeAreaId = new Guid("09e1c9bf-a5ba-4e3e-8c7f-cd36a45c8feb");
				public static readonly Guid ControlV2ComboBoxDirectionId = new Guid("8697b3ab-a525-415c-8107-58c8618c4137");
				public static readonly Guid ControlSwitchSearchAroundId = new Guid("29c7f704-7df2-49f3-b927-44b63bb07d91");
				public static readonly Guid ControlRANGELabelId = new Guid("05ecf464-5827-43d9-9a26-622175aa3492");
				public static readonly Guid ControlTITRELabelId = new Guid("e6a9de59-6d8b-4d68-adfb-539af49526cd");
				public static readonly Guid ControlBoxViewId = new Guid("41f08920-d3de-4748-a9d6-f3c3ff6b6056");
				public static readonly Guid ControlDIRECTIONLabelId = new Guid("8890d3b7-ae4f-4885-b681-b89b612b5a9c");
				public static readonly Guid ControlSEARCHAROUNDLabelId = new Guid("a4bd0f73-2075-4a59-8628-9c89ec014ac9");
				public static readonly Guid ControltriangleImagecloneId = new Guid("c8281cab-765a-4fd5-9a5f-a638be5bac0a");
			}
			public static class HomeNotificationsCell
			{
				public static readonly Guid Id = new Guid("273ac374-7c5f-4b87-ba7b-cb861874242e");
				public static readonly Guid ControlbgblancBoxViewId = new Guid("2ac6bb47-a84a-4c43-ab10-23fe05691c67");
				public static readonly Guid ControlBoxViewId = new Guid("40f1ea3e-7100-4bcf-98d9-650f7f5e16dc");
				public static readonly Guid ControlpointImageId = new Guid("722f4d68-dae5-4213-8d1b-2480a74d86bc");
				public static readonly Guid ControlDATELabelId = new Guid("34c28490-7173-4303-abac-2dfc66911a33");
				public static readonly Guid ControlDESCRIPTIONLabelId = new Guid("1d463654-7750-442f-8788-b5795583eae2");
			}
			public static class MissionCargoorCharterCell
			{
				public static readonly Guid Id = new Guid("261d18a3-62f8-4248-9cfa-fb7039f7f2ea");
				public static readonly Guid ControlBGBLANCBoxViewId = new Guid("edb57851-a2ab-49d6-a065-240281f38a44");
				public static readonly Guid ControlGAUCHEBoxViewId = new Guid("aa21a14d-de2a-4b33-b1fc-ddbcbd6c00c7");
				public static readonly Guid ControlMILIEUBoxViewId = new Guid("2bd8cc91-ffcf-4e72-9534-c8bafdb06f92");
				public static readonly Guid ControlTITRELabelId = new Guid("bad6fca0-6892-4e8c-aa5f-b9e925795ac9");
				public static readonly Guid ControlHEADINGImageId = new Guid("fe99bfef-3213-4d71-9f97-3c130a7cb350");
				public static readonly Guid ControlHEADINGLabelId = new Guid("4a47677e-e2b6-4691-b04e-6288f938b609");
				public static readonly Guid ControlDISTANCEImageId = new Guid("257d77e3-5fe2-4263-9acd-7e56c3c41956");
				public static readonly Guid ControlDISTANCELabelId = new Guid("778d041a-3b69-41f3-9e4f-c0c43e452c0d");
				public static readonly Guid ControlCARGOImageId = new Guid("122882b6-79d4-4340-8346-473e20f9e4cd");
				public static readonly Guid ControlCARGOLabelId = new Guid("6286bd48-2072-4b1c-ba35-1ee15ba5ab1d");
				public static readonly Guid ControlPAXImageId = new Guid("5340a2a4-ce02-4374-8c8b-f2b1b7586072");
				public static readonly Guid ControlPLANEImageId = new Guid("3cf8fdde-c755-4c02-a1e9-47e5575a3938");
				public static readonly Guid ControlICAOFROMLabelId = new Guid("8c074ea9-6aa9-440a-b740-fb9e1288bcda");
				public static readonly Guid ControlICAOTOLabelId = new Guid("06b91179-56db-4d51-8d2e-3cfaffecea9a");
				public static readonly Guid ControlFROMLabelId = new Guid("4e62c193-73b9-4ef2-91a8-d0dbda64f4e8");
				public static readonly Guid ControlTOLabelcloneId = new Guid("37eb1309-82b8-4d8b-856b-eb1678b60928");
				public static readonly Guid ControlCONSTRAINTEBYTIMEImageId = new Guid("bc083958-0e80-41fb-9e03-c989a18c5a98");
				public static readonly Guid ControlCONSTRAINTEBYTIMELabelId = new Guid("3a70d4de-4d69-495f-a22f-7ee6f9a89840");
			}
			public static class MissionTabMAP
			{
				public static readonly Guid Id = new Guid("28f404f3-c8a6-4a1b-9b6c-5a45c1405e60");
				public static readonly Guid ControlBoxViewNavigationBarcloneId = new Guid("459a0e40-0c66-4171-9184-d0adb90d858c");
				public static readonly Guid ControlButtonTakeMissionId = new Guid("d421cb30-24a3-4321-9e93-cf3f846d88ed");
				public static readonly Guid ControlActivityIndicatorId = new Guid("4e2da313-36d4-434f-abfb-944b67b80348");
				public static readonly Guid ControlFONDBLANCBoxViewId = new Guid("2eea2e17-cb36-462e-bd66-57c56a402f19");
				public static readonly Guid ControlGAUCHEBoxViewcloneId = new Guid("08f9b925-e263-45ac-95bd-591f23d23101");
				public static readonly Guid ControlDROITEBoxViewcloneId = new Guid("4c231a4d-ff59-400a-bf1f-82528a6c6bf1");
				public static readonly Guid ControlHEADINGImagecloneId = new Guid("9e62c5e1-f332-4be3-84dc-2051899bd3a8");
				public static readonly Guid ControlHEADINGLabelcloneId = new Guid("d3d990f1-75ba-4f02-9f4b-8bd44798f668");
				public static readonly Guid ControlDISTANCEImagecloneId = new Guid("e4b1892a-f9e2-4de4-9809-030a3740683b");
				public static readonly Guid ControlDISTANCELabelcloneId = new Guid("97312061-e13a-46c8-83f9-84b6188c8ba4");
				public static readonly Guid ControlMILIEUBoxViewcloneId = new Guid("486ee0d7-19fb-4619-bc4a-0da01bf3aaef");
				public static readonly Guid ControlTITRELabelcloneId = new Guid("cbc85d6e-d409-4c45-99b2-e7bab12333b2");
				public static readonly Guid ControlEXPIRATIONDATEBoxViewcloneId = new Guid("431025be-950f-4259-8466-c277b3a0c399");
				public static readonly Guid ControlEXPIRATIONDATELabelcloneId = new Guid("a13b5234-4147-4adc-b1cb-9745ae459219");
				public static readonly Guid ControlCARGOImagecloneId = new Guid("0b344836-33c6-4e57-807e-bdc5e068dc3c");
				public static readonly Guid ControlCARGOLabelcloneId = new Guid("b03316e0-3977-42f1-af65-67031c1a81a7");
				public static readonly Guid ControlPAXImagecloneId = new Guid("c7d2879e-6e7e-478f-b793-d09ad9347c24");
				public static readonly Guid ControlPAXLabelcloneId = new Guid("0e5d6c79-0b2b-4bf5-9d3d-7b7884471f1b");
				public static readonly Guid ControlXPLabelcloneId = new Guid("73120b3c-c5bb-4c46-99f3-204225be9e3d");
				public static readonly Guid ControlXPRESULTATLabelcloneId = new Guid("e32a7630-f0ec-4371-ab5e-56e964d1ad1e");
				public static readonly Guid ControlPENALTYImagecloneId = new Guid("815ed797-51b9-4cb6-89f0-e5c219abadde");
				public static readonly Guid ControlPENALTYLabelcloneId = new Guid("49758f29-b4a9-4872-ac5d-c0173b02d6f3");
				public static readonly Guid ControlPAYImagecloneId = new Guid("66e8d48f-2529-42f1-aad1-30320385c6e8");
				public static readonly Guid ControlPAYLabelcloneId = new Guid("cc2601f3-e460-4d1d-ab17-dc89b15679e5");
				public static readonly Guid ControlCONSTRAINTEBYTIMEImagecloneId = new Guid("02269fe2-c9d8-4909-a6d6-d73c3256437c");
				public static readonly Guid ControltriangleImagecloneId = new Guid("b7887f5f-f995-4c95-8e54-fe0e859c81d3");
				public static readonly Guid ControlButtonInfosId = new Guid("6fa60d23-09e6-40d0-8e8c-190eb1923829");
				public static readonly Guid ControlBASELabelId = new Guid("0940a73d-c5cd-4b04-8d79-1b81f8691544");
				public static readonly Guid ControlBASERESULTLabelId = new Guid("ae84e18a-ba23-46db-91c9-74443074cdec");
				public static readonly Guid ControlCOINPENALTYImagecloneId = new Guid("ef25273a-e410-4d4a-a43a-6d9d634df96c");
				public static readonly Guid ControlCOINPENALTYImageclonecloneId = new Guid("24993ef8-bd9f-4688-93e8-c69846e822be");
				public static readonly Guid ControlV2MapId = new Guid("2bc52678-adf0-48d3-b8aa-491cfa8691b0");
			}
			public static class Pagedechargement
			{
				public static readonly Guid Id = new Guid("c4eae052-5ee4-4445-b320-7c94dcb3310f");
				public static readonly Guid ControlImageWebId = new Guid("c7564a39-f0c7-42bb-b7d4-1ed83ad12335");
				public static readonly Guid ControlBoxViewId = new Guid("4fdd69bf-363b-4885-9f52-945248454b3c");
				public static readonly Guid ControlRETRYButtonId = new Guid("93d1fc17-64fb-4c0f-8a10-9716a267633c");
				public static readonly Guid ControlSTATUSLabelId = new Guid("8f6d7fb8-ed96-4655-92e8-52f13467b16c");
				public static readonly Guid ControlActivityIndicatorId = new Guid("7071d3ec-d2f2-49ba-9568-55e5dfa5d0e0");
			}
			public static class Notifications
			{
				public static readonly Guid Id = new Guid("d2630f15-fcde-4eac-9db3-58ba7c1a800f");
				public static readonly Guid ControlImageId = new Guid("e19fec86-ac95-46b0-a1f8-1e3a87ca0a7c");
				public static readonly Guid ControlV2ListViewId = new Guid("35aa397a-67e2-4470-8c72-816e8e1d9c91");
			}
			public static class SignInScrollViewContent
			{
				public static readonly Guid Id = new Guid("4aa47af2-e262-448e-b051-277ceb308aa4");
				public static readonly Guid ControlImageBGbleuId = new Guid("c929d8ac-5a97-478c-b32d-dbd858f15640");
				public static readonly Guid ControlRondblancBoxViewId = new Guid("1b061bd3-e815-40c5-a03a-bfdfe056f325");
				public static readonly Guid ControlLogoImageId = new Guid("356100ea-550c-44fd-bc45-e12d7da05d0f");
				public static readonly Guid ControlLabelId = new Guid("03dff840-f027-4087-8045-6e8900b35b47");
				public static readonly Guid ControlEntryEmailId = new Guid("f74b08e3-ef78-47ab-8736-0dac0cc13d82");
				public static readonly Guid ControlEntryPasswordId = new Guid("0891c565-c0b2-435e-a2d7-3bafaa437f51");
				public static readonly Guid ControlActivityIndicatorId = new Guid("91859b23-0052-4fca-86e8-0e9279be44f8");
				public static readonly Guid ControlLabelStatusId = new Guid("7eb2229d-77dc-463c-bbf7-e918c4948cc8");
				public static readonly Guid ControlBGBoutonImageId = new Guid("243b2319-f7a2-48a3-b856-4012eecc2c27");
				public static readonly Guid ControlButtonLoginId = new Guid("1f932b8e-5d48-498a-aa89-120b08a985b7");
				public static readonly Guid ControlemailLabelId = new Guid("62d190cc-c2e3-4aaf-8678-a29b7a53bb54");
				public static readonly Guid ControlmdpLabelId = new Guid("5da81ac4-f6ca-41f9-aa5e-3d3ce289b98b");
				public static readonly Guid ControlEMAILImageId = new Guid("875fab5b-3720-4588-9c3d-2ee209e56cfd");
				public static readonly Guid ControlImageId = new Guid("b3b2adb5-ec8e-4b03-b883-eb4ba3951756");
			}
			public static class MissionsCellclone
			{
				public static readonly Guid Id = new Guid("8f0481e0-0bcc-4c7e-a00a-e6439e3c48fd");
				public static readonly Guid ControlBGBLANCBoxViewId = new Guid("49e2499a-f7c3-4a62-946b-158a46bc3885");
				public static readonly Guid ControlGAUCHEBoxViewId = new Guid("cb04448b-2265-4f9a-9ee6-cbbde994982a");
				public static readonly Guid ControlMILIEUBoxViewId = new Guid("8ee93ba1-0657-4ed8-ad06-490271134b15");
				public static readonly Guid ControlDROITEBoxViewId = new Guid("c4906a2f-431d-45af-bf40-a043dcf196dd");
				public static readonly Guid ControlTITRELabelId = new Guid("2cb8e792-66cf-4ae2-b27c-bd7051694523");
				public static readonly Guid ControlHEADINGImageId = new Guid("7a20b3ab-227e-4f24-a12a-b58709a785a6");
				public static readonly Guid ControlHEADINGLabelId = new Guid("dabab953-6b52-4cbe-b1da-4cad0e9af94e");
				public static readonly Guid ControlDISTANCEImageId = new Guid("7c7e8962-da50-4ba0-bc08-0c1625888118");
				public static readonly Guid ControlDISTANCELabelId = new Guid("9836c723-4884-4d13-930d-ddc74cf937e6");
				public static readonly Guid ControlCARGOImageId = new Guid("6acf8ccc-70e5-4f1b-bd1f-7b29bb5f38e5");
				public static readonly Guid ControlCARGOLabelId = new Guid("806ae91e-68ed-4bc0-bf79-172c4aee94f0");
				public static readonly Guid ControlPAXImageId = new Guid("f94aa1e0-dd19-4fa7-94e7-f74799409f9d");
				public static readonly Guid ControlPAXLabelId = new Guid("ca4d4c82-ee39-4fb6-baf2-6927c15dc9f7");
				public static readonly Guid ControlPENALTYImageId = new Guid("8cef52fd-8d5a-4750-a2d2-43aef1d6f4c5");
				public static readonly Guid ControlPENALTYLabelId = new Guid("a1ae2b0f-2700-46bc-91c8-1f872c5c77d3");
				public static readonly Guid ControlPAYLabelId = new Guid("5b936e6c-1243-4e62-bb21-24b9f5f13ba2");
				public static readonly Guid ControlPAYImageId = new Guid("4c4e1a94-ffd3-4468-9ab6-9798bd377885");
				public static readonly Guid ControlCONSTRAINTEBYTIMEImageId = new Guid("4fefd5e1-a64d-4751-b499-b0e4532c6dfb");
				public static readonly Guid ControlEXPIRATIONDATEBoxViewId = new Guid("47bff499-8b04-46c9-aed1-61bfa52d1aa9");
				public static readonly Guid ControlEXPIRATIONDATELabelId = new Guid("3e34f367-26c5-4967-985c-2d01bf0b8327");
				public static readonly Guid ControlBASELabelId = new Guid("661961de-bc25-4141-b926-f5904d65f258");
				public static readonly Guid ControlICAOLabelId = new Guid("983a42c1-a22d-462f-bf45-876dbd2a2cab");
				public static readonly Guid ControlEXPLabelId = new Guid("d60beae0-d646-4090-9f0c-6eb5807772c4");
				public static readonly Guid ControlCOINPENALTYImageId = new Guid("447d3b2a-ed3b-4a27-ba4e-9c2fde8715dc");
				public static readonly Guid ControlCOINPAYImageId = new Guid("79387f4b-2773-40ce-8d56-67866ee96a68");
				public static readonly Guid ControlICAOLabel1Id = new Guid("5d059d1f-7510-4c81-b981-826c12978c16");
				public static readonly Guid ControlImageId = new Guid("eb3680e1-da47-48d2-b530-0dfb7a2f4b83");
			}
			public static class DetailnewsscrollNewPage
			{
				public static readonly Guid Id = new Guid("e2f0ff6f-7ec5-4871-a91c-9bfe4d471fa1");
				public static readonly Guid ControlScrollId = new Guid("4c0a81df-ca25-41ba-92e9-b0695d423010");
			}
		}
	}
}
