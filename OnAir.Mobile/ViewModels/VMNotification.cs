﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMNotification : BaseViewModel
    {
        public string Description { get; set; }
        public string Date { get; set; }

        public VMNotification(WSNotification n)
        {
            Description = n.Description;
            Date = n.Date.ToString(Strings.DATE_FORMAT_1);
        }
    }
}
