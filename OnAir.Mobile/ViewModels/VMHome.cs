﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMHome : BaseViewModel
    {
        private static VMHome _instance;
        public static VMHome Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new VMHome();
                }
                return _instance;
            }
        }


        private bool _showLoader;
        public bool ShowLoader
        {
            get { return _showLoader; }
            set { _showLoader = value; OnPropertyChanged(nameof(ShowLoader)); }
        }

        private string _loaderStatus;
        public string LoaderStatus
        {
            get { return _loaderStatus; }
            set { _loaderStatus = value; OnPropertyChanged(nameof(LoaderStatus)); }
        }

        private bool _showNotLoggedMessage;
        public bool ShowNotLoggedMessage
        {
            get { return _showNotLoggedMessage; }
            set { _showNotLoggedMessage = value; OnPropertyChanged(nameof(ShowNotLoggedMessage)); }
        }


        private bool _showNotifications;
        public bool ShowNotifications
        {
            get { return _showNotifications; }
            set { _showNotifications = value; OnPropertyChanged(nameof(ShowNotifications)); }
        }

        private WSGetWorldsResult.World _selectedWorld;
        public WSGetWorldsResult.World SelectedWorld
        {
            get { return _selectedWorld; }
            set { _selectedWorld = value; OnPropertyChanged(nameof(SelectedWorld)); }
        }

        public ObservableRangeCollection<VMNews> News { get; set; }
        public ObservableRangeCollection<VMNotification> NotificationsMore { get; set; }
        public ObservableRangeCollection<VMNotification> Notifications { get; set; }
        public Ecommunity.Mobile.Action RefreshNotificationsAction { get; set; }
        public Ecommunity.Mobile.Action NewsClickedAction { get; set; }

        public List<WSGetWorldsResult.World> Worlds { get; set; }

        public VMHome()
        {
            News = new ObservableRangeCollection<VMNews>();
            Notifications = new ObservableRangeCollection<VMNotification>();
            NotificationsMore = new ObservableRangeCollection<VMNotification>();

            RefreshNotificationsAction = new Ecommunity.Mobile.Action((control, context) =>
            {
                LoadNewsAndNotifs();
            });

            NewsClickedAction = new Ecommunity.Mobile.Action((control, context) => 
            {
                var vm = context as VMNews;
                Services.UriHandler.HandleUriAsync(vm.Permalink, null);
            });

            BUSConf.LoadConf();
            Conf.Load();
            BUSAuth.LoadAccessToken();
            LoadNewsAndNotifs();
        }


        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(SelectedWorld) && SelectedWorld != null)
            {
                BUSConf.Conf.SelectedWorldId = SelectedWorld.Id;
                BUSConf.SaveConf();
                LoadNewsAndNotifs();
            }
        }

        public void LoadNewsAndNotifs()
        {
            ShowNotLoggedMessage = false;
            ShowNotifications = false;

            ShowLoader = true;
            LoaderStatus = Strings.HOME_LOADING;

            var t = new Thread(() => 
            {
                try
                {
                    var worldsResult = WS.GetWorlds(new WSParam() { AccessToken = BUSAuth.AccessToken });
                    if (worldsResult.IsSuccess)
                    {
                        Worlds = worldsResult.Worlds.OrderBy(w => w.Id).ToList();
                    }
                    else
                    {
                        LoaderStatus = worldsResult.Error;
                    }

                    _selectedWorld = Worlds.FirstOrDefault(o => o.Id == BUSConf.Conf.SelectedWorldId);
                    if (_selectedWorld == null)
                    {
                        _selectedWorld = Worlds.First();
                    }

                    var news = WS.GetNews();
                    if (news.IsSuccess)
                    {
                        var cultureInfo = new CultureInfo("en-US");
                        var vms = news.PublishedNews.Select(n => new VMNews(n, cultureInfo)).OrderByDescending(n => n.PublishDate).Take(5);
                        Device.BeginInvokeOnMainThread(() => 
                        {
                            News.Clear();
                            News.AddRange(vms);
                        });
                    }
                    else
                    {
                        LoaderStatus = news.Error;
                    }

                    if (BUSAuth.IsLoggedIn())
                    {
                        var companyInfos = WS.GetCompanyInfos(new WSGetCompanyInfos2Params()
                        {
                            WorldId = SelectedWorld.Id
                        });

                        if (companyInfos.IsSuccess)
                        {
                            var notifs = WS.GetNotifications(new WSGetNotificationsParam()
                            {
                                AccessToken = BUSAuth.AccessToken,
                                CompanyId = companyInfos.Id
                            });

                            if (notifs.IsSuccess)
                            {
                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    NotificationsMore.Clear();
                                    NotificationsMore.AddRange(notifs.Notifications.OrderByDescending(n => n.Date).Select(n => new VMNotification(n)));

                                    Notifications.Clear();
                                    Notifications.AddRange(NotificationsMore.Take(5));
                                });
                                LoaderStatus = string.Empty;
                                ShowNotLoggedMessage = BUSAuth.IsLoggedIn() == false;
                                ShowNotifications = !ShowNotLoggedMessage;
                            }
                            else
                            {
                                LoaderStatus = notifs.Error;
                            }
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                NotificationsMore.Clear();
                                Notifications.Clear();
                            });
                            LoaderStatus = companyInfos.Error;
                        }

                    }
                    else
                    {
                        LoaderStatus = string.Empty;
                        ShowNotLoggedMessage = BUSAuth.IsLoggedIn() == false;
                        ShowNotifications = !ShowNotLoggedMessage;
                    }
                }
                catch (Exception ex)
                {
                    LoaderStatus = Strings.HOME_LOADING_FAIL;
                }

                ShowLoader = false;
            });

            t.Start();
        }
    }
}
