﻿using Ecommunity.Mobile;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMAuth : Ecommunity.Mobile.BaseViewModel
    {
        private bool _showLoader;
        public bool ShowLoader
        {
            get { return _showLoader; }
            set { _showLoader = value; OnPropertyChanged(nameof(ShowLoader)); }
        }

        private string _loaderStatus;
        public string LoaderStatus
        {
            get { return _loaderStatus; }
            set { _loaderStatus = value; OnPropertyChanged(nameof(LoaderStatus)); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; OnPropertyChanged(nameof(Email)); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; OnPropertyChanged(nameof(Password)); }
        }

        private bool _enableLoginButton;
        public bool EnableLoginButton
        {
            get { return _enableLoginButton; }
            set { _enableLoginButton = value; OnPropertyChanged(nameof(EnableLoginButton)); }
        }


        public Ecommunity.Mobile.Action LoginAction { get; set; }

        private Guid _redirectPageId;

        public VMAuth() : this(Ids.phone320568.Home.Id)
        {

        }

        public VMAuth(Guid redirectPageId)
        {
            _redirectPageId = redirectPageId;

            if (EcommunitySettings.Channel == Channel.Beta)
            {
                Email = "arthur@onair.company";
                Password = "okjv1234";
            }
            else
            {
                Email = "";
                Password = "";
            }

            EnableLoginButton = true;
            LoginAction = new Ecommunity.Mobile.Action((control, context) => { Login(); });
        }

        private void Login()
        {
            ShowLoader = true;
            EnableLoginButton = false;
            LoaderStatus = Strings.AUTH_LOGING;

            var t = new Thread(() =>
            {
                try
                {
                    BUSAuth.Login(Email, Password);
                    LoaderStatus = Strings.AUTH_LOGING_SUCCESS;

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (_redirectPageId == Ids.phone320568.Home.Id)
                        {
                            Ecommunity.Mobile.EcommunityMobile.MainPage.PopToRootAsync(true);
                        }
                        else
                        {
                            Ecommunity.Mobile.EcommunityMobile.Navigate(_redirectPageId, null, true);
                        }
                    });
                }
                catch (Exception ex)
                {
                    LoaderStatus = Strings.AUTH_LOGING_FAIL + " " + ex.Message;
                }

                ShowLoader = false;
                EnableLoginButton = true;
            });

            t.Start();
        }
    }
}
