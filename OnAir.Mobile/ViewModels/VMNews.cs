﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMNews : BaseViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Link { get; set; }
        public DateTime PublishDate { get; set; }
        public DateTime? UnpublishDate { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
        public string Permalink { get; set; }
        public string PublishDateDisplay { get; set; }
        public string Chapo { get; set; }
        public string Auteur { get; set; }
        public string VideoUrl { get; set; }
        public bool ShowVideo { get; set; }
        public string Categorie { get; set; }

        public VMNews(WSLauncherGetNewsResult.News n, CultureInfo ci)
        {
            Id = n.Id;
            Title = n.Title;
            Subtitle = n.Subtitle;
            Link = n.Permalink;
            PublishDate = n.PublishDate;
            UnpublishDate = n.UnpublishDate;
            Content = n.Content;
            ImageUrl = n.ImageUrl;
            Permalink = n.Permalink;
            PublishDateDisplay = PublishDate.ToString(Strings.DATE_FORMAT_2, ci);
            Chapo = n.Chapo;
            Auteur = n.Auteur;
            VideoUrl = n.VideoUrl;
            ShowVideo = !string.IsNullOrEmpty(n.VideoUrl);
            Categorie = n.Categorie;
        }
    }
}
