﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    public class VMComboBoxItem : BaseViewModel
    {
        public int Value { get; set; }
        public string Title { get; set; }

        public static List<VMComboBoxItem> GetDirections()
        {
            var result = new List<VMComboBoxItem>();

            result.Add(new VMComboBoxItem()
            {
                Value = (int)MissionDirectionShared.From,
                Title = "From"
            });

            result.Add(new VMComboBoxItem()
            {
                Value = (int)MissionDirectionShared.To,
                Title = "To"
            });

            return result;
        }

        public static List<VMComboBoxItem> GetRangeAreas()
        {
            var result = new List<VMComboBoxItem>();

            result.Add(new VMComboBoxItem()
            {
                Value = 0,
                Title = "Very Short (<50 NM)"
            });

            result.Add(new VMComboBoxItem()
            {
                Value = 1,
                Title = "Small Trip (50 to 250 NM)"
            });

            result.Add(new VMComboBoxItem()
            {
                Value = 2,
                Title = "Short Haul (250 to 1000 NM)"
            });

            result.Add(new VMComboBoxItem()
            {
                Value = 3,
                Title = "Medium Haul (1000 to 3000 NM)"
            });

            result.Add(new VMComboBoxItem()
            {
                Value = 4,
                Title = "Long Haul (>3000 NM)"
            });

            return result;
        }
    }
}
