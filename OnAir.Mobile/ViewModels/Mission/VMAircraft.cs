﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMAircraft : BaseViewModel
    {
        public string Identifier { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }


        public VMAircraft(WSServerMission.Aicraft a)
        {
            Identifier = a.Identifier;
            Latitude = a.Latitude;
            Longitude = a.Longitude;
        }
    }
}
