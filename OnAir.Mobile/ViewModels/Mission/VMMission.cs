﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMMission : BaseViewModel
    {
        private bool _showLoader;
        public bool ShowLoader
        {
            get { return _showLoader; }
            set { _showLoader = value; OnPropertyChanged(nameof(ShowLoader)); }
        }

        public Guid Id { get; set; }
        public string Description { get; set; }
        public string Heading { get; set; }
        public string Distance { get; set; }
        public string CargoTotalWeightLbs { get; set; }
        public string PAXNumber { get; set; }
        public string ExpirationDate { get; set; }
        public Color ExpirationDateColor { get; set; }
        public string XP { get; set; }
        public string Penalty { get; set; }
        public string Pay { get; set; }
        public bool ConstraintTime { get; set; }
        public Color ConstraintTimeColor { get; set; }
        public bool ConstraintHumanPilot { get; set; }
        public bool CanAccess { get; set; }
        public bool ShowTakeButton { get; set; }
        public string CanAccessDisplay { get; set; }
        public string MainAirportICAO { get; set; }

        public bool ShowCargos { get; set; }
        public bool ShowPAXs { get; set; }
        public bool ShowInfoButton { get; set; }

        public List<VMCargoOrCharter> CargoAndCharters { get; set; }
        public ObservableRangeCollection<VMMapItem> MapItems { get; set; }
        public ObservableRangeCollection<LineString> LineStrings { get; set; }

        public Ecommunity.Mobile.Action TakeMissionCommand { get; set; }
        public Ecommunity.Mobile.Action SelectCargoOrCharterCommand { get; set; }
        public Ecommunity.Mobile.Action InfoButtonCommand { get; set; }

        private WSServerMission _model;
        private VMCargoOrCharter _selectedCargoOrCharter;

        public VMMission(WSServerMission m)
        {
            _model = m;

            MapItems = new ObservableRangeCollection<VMMapItem>();
            LineStrings = new ObservableRangeCollection<LineString>();

            Id = m.Id;
            Description = m.Description;
            Heading = string.Format("{0}°", m.MainAirportHeading);
            Distance = string.Format("{0:N0} NM", m.TotalDistance);
            MainAirportICAO = m.MainAirport?.ICAO;

            if (m.Cargos.Length > 0)
            {
                CargoTotalWeightLbs = string.Format("{0:N0} lbs", m.Cargos.Sum(c => c.Weight));
                ShowCargos = true;
            }

            if (m.Charters.Length > 0)
            {
                PAXNumber = string.Format("{0:N0} PAX", m.Charters.Sum(c => c.PassengersNumber));
                ShowPAXs = true;
            }

            if (m.ExpirationDate.HasValue)
            {
                var expirationTimeSpan = m.ExpirationDate.Value - DateTime.UtcNow;
                ExpirationDate = TimeSpanDisplay(expirationTimeSpan);
            }
            else
            {
                ExpirationDate = "Never expires";
            }
            ExpirationDateColor = RemainingTimeColor(m.ExpirationDate);

            XP = m.XP.ToString();
            Penalty = BUSFormat.Amount(m.Penality);
            Pay = BUSFormat.Amount(m.Pay);
            ConstraintTime = (m.Cargos != null && m.Cargos.Any(c => c.CargoAvailableTime.HasValue)) || (m.Charters != null && m.Charters.Any(c => c.CharterAvailableTime.HasValue));

            if (ConstraintTime)
            {
                DateTime? worstCargoDate = null;
                DateTime? worstCharterDate = null;
                var worstTime = new DateTime();

                if (m.Cargos.Count(c => c.CargoAvailableTime.HasValue) > 0)
                {
                    worstCargoDate = m.Cargos.Where(c => c.CargoAvailableTime.HasValue).Min(c => c.CargoAvailableTime);
                }

                if (m.Charters.Count(c => c.CharterAvailableTime.HasValue) > 0)
                {
                    worstCharterDate = m.Charters.Where(c => c.CharterAvailableTime.HasValue).Min(c => c.CharterAvailableTime);
                }

                if (worstCargoDate.HasValue && worstCharterDate.HasValue == false)
                {
                    worstTime = worstCargoDate.Value;
                }
                else if (worstCargoDate.HasValue == false && worstCharterDate.HasValue)
                {
                    worstTime = worstCharterDate.Value;
                }
                else
                {
                    worstTime = worstCargoDate.Value < worstCharterDate.Value ? worstCargoDate.Value : worstCharterDate.Value;
                }

                ConstraintTimeColor = GetConstraintTimeColor(worstTime);
            }

            ConstraintHumanPilot = (m.Cargos != null && m.Cargos.Any(c => c.HumanOnly)) || (m.Charters != null && m.Charters.Any(c => c.HumanOnly));

            CargoAndCharters = new List<VMCargoOrCharter>();
            CargoAndCharters.AddRange(m.Cargos.Select(c => new VMCargoOrCharter(c)).ToList());
            CargoAndCharters.AddRange(m.Charters.Select(c => new VMCargoOrCharter(c)).ToList());

            CanAccess = m.CanAccess && m.TakenDate == null;
            CanAccessDisplay = m.CanAccessDisplay;
            ShowTakeButton = m.TakenDate == null;
            ShowInfoButton = CanAccess == false && ShowTakeButton;

            InfoButtonCommand = new Ecommunity.Mobile.Action((control, context) => { InfoButton(); });

            TakeMissionCommand = new Ecommunity.Mobile.Action((control, context) => { TakeMission(); });

            SelectCargoOrCharterCommand = new Ecommunity.Mobile.Action((control, context) => { SelectCargoOrCharter(context); });

            InitMap();
        }

        public WSServerMission GetModel()
        {
            return _model;
        }

        private void InitMap()
        {
            var airports = new List<VMAirport>();
            var linestrings = new List<LineString>();

            var mapItems = new List<VMMapItem>();
            var cargosAndCharters = new List<VMMapItem>();

            foreach (var c in CargoAndCharters)
            {
                var lat = 0.0;
                var lng = 0.0;
                var loc = string.Empty;

                if (c.AircraftCurrent != null)
                {
                    lat = c.AircraftCurrent.Latitude;
                    lng = c.AircraftCurrent.Longitude;
                    loc = c.AircraftCurrent.Identifier;
                }
                else if (c.AirportCurrent != null)
                {
                    lat = c.AirportCurrent.Latitude;
                    lng = c.AirportCurrent.Longitude;
                    loc = c.AirportCurrent.ICAO;
                }

                if (c.AirportTo != null)
                {
                    if (airports.All(a => a.ICAO != c.AirportTo.ICAO))
                    {
                        airports.Add(c.AirportTo);
                    }
                }

                if (lat != -1000 && lng != -1000)
                {
                    cargosAndCharters.Add(new VMMapItem()
                    {
                        Title = c.CargoOrPAXValue,
                        Latitude = lat,
                        Longitude = lng,
                        Location = loc,
                        Type = c.IsCargo ? MapItemType.Cargo : MapItemType.Charter
                    });

                    if (c.AirportCurrent?.ICAO != c.AirportTo.ICAO)
                    {
                        var coords = new List<Coordinate>();
                        coords.Add(new Coordinate()
                        {
                            Latitude = lat,
                            Longitude = lng
                        });
                        coords.Add(new Coordinate()
                        {
                            Latitude = c.AirportTo.Latitude,
                            Longitude = c.AirportTo.Longitude
                        });
                        linestrings.Add(new LineString()
                        {
                            Clickable = false,
                            Color = Color.Red,
                            Width = 5,
                            Coordinates = coords
                        });
                    }
                }
            }

            var groups = cargosAndCharters.GroupBy(c => c.Location).ToList();
            foreach (var group in groups)
            {
                var lat = group.First().Latitude;
                var lng = group.First().Longitude;
                var title = string.Join(", ", group.Select(g => g.Title));

                ImageSource icon = null; 

                if (group.All(g => g.Type == MapItemType.Cargo))
                {
                    icon = new AppResourceReference()
                    {
                        Uri = "ecommunity://app_resources/map/pin_cargo.png"
                    }.ToImageSource();
                }
                else if (group.All(g => g.Type == MapItemType.Charter))
                {
                    icon = new AppResourceReference()
                    {
                        Uri = "ecommunity://app_resources/map/pin_charter.png"
                    }.ToImageSource();
                }
                else
                {
                    icon = new AppResourceReference()
                    {
                        Uri = "ecommunity://app_resources/map/pin_cargo_and_charter.png"
                    }.ToImageSource();
                }

                mapItems.Add(new VMMapItem()
                {
                    Latitude = lat,
                    Longitude = lng,
                    Title = title,
                    Icon = icon
                });
            }

            mapItems.AddRange(airports.Select(a => new VMMapItem()
            {
                Latitude = a.Latitude,
                Longitude = a.Longitude,
                Title = a.ICAO,
                Type = MapItemType.Airport,
                Icon = new AppResourceReference()
                {
                    Uri = "ecommunity://app_resources/map/pin_arrival.png"
                }.ToImageSource()
            }));

            Device.BeginInvokeOnMainThread(() => 
            {
                MapItems.Clear();
                MapItems.AddRange(mapItems);

                LineStrings.Clear();
                LineStrings.AddRange(linestrings);
            });
        }

        private void SelectCargoOrCharter(object context)
        {
            var vm = context as VMCargoOrCharter;
            if (_selectedCargoOrCharter != null)
            {
                _selectedCargoOrCharter.IsSelected = false;
            }

            vm.IsSelected = true;
            _selectedCargoOrCharter = vm;
        }

        private void InfoButton()
        {
            EcommunityMobile.MainPage.DisplayAlert(Strings.MISSION_INFO_TITLE, _model.CanAccessDisplay, Strings.OK);
        }

        private void TakeMission()
        {
            if (ShowLoader)
            {
                return;
            }

            ShowLoader = true;

            var t = new Thread(() => 
            {
                WSResult result = null;

                try
                {
                    result = WS.TakeMission(new WSServerTakeMissionParam()
                    {
                        CompanyId = VMMissions.CompanyId,
                        MissionId = Id
                    });
                }
                catch (Exception)
                {

                }

                Device.BeginInvokeOnMainThread(() =>
                {
                    if (result == null || result.IsSuccess == false)
                    {
                        var message = result?.Error != null ? result.Error : Strings.MISSION_TAKE_FAIL_ALERT_MESSAGE;
                        EcommunityMobile.MainPage.DisplayAlert(Strings.MISSION_TAKE_FAIL_ALERT_TITLE, message, Strings.OK);
                    }
                    else if (result.IsSuccess)
                    {
                        EcommunityMobile.MainPage.DisplayAlert(Strings.MISSION_TAKE_SUCCESS_ALERT_TITLE, Strings.MISSION_TAKE_SUCCESS_ALERT_MESSAGE, Strings.OK);
                        EcommunityMobile.MainPage.PopAsync();
                    }
                });

                ShowLoader = false;
            });
            t.Start();
        }

        public static string TimeSpanDisplay(TimeSpan span)
        {
            string formatted;
            if (span > new System.TimeSpan(1, 0, 0, 0))
                formatted = string.Format("{0}{1}",
                   span.Duration().Days > 0 ? string.Format("{0:0} day{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
                   span.Duration().Hours > 0 ? string.Format("{0:0} hour{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty);
            else
                formatted = string.Format("{0}{1}{2}",
                   span.Duration().Days > 0 ? string.Format("{0:0} day{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
                   span.Duration().Hours > 0 ? string.Format("{0:0} hour{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
                   span.Duration().Minutes > 0 ? string.Format("{0:0} minute{1}, ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 seconds";

            return formatted;
        }

        public static Color RemainingTimeColor(DateTime? expirationDate)
        {
            if (expirationDate.HasValue)
            {
                var ts = expirationDate.Value - DateTime.UtcNow;
                Color cl;
                if (ts > new System.TimeSpan(7, 0, 0, 0))
                    cl = Color.Cyan;
                else if (ts > new System.TimeSpan(5, 0, 0, 0))
                    cl = Color.Green;
                else if (ts > new System.TimeSpan(2, 0, 0, 0))
                    cl = Color.YellowGreen;
                else if (ts > new System.TimeSpan(1, 0, 0, 0))
                    cl = Color.Yellow;
                else if (ts > new System.TimeSpan(0, 12, 0, 0))
                    cl = Color.Orange;
                else if (ts > new System.TimeSpan(0, 3, 0, 0))
                    cl = Color.Red;
                else if (ts > new System.TimeSpan(0, 1, 0, 0))
                    return Color.FromHex("#7013CE");
                else
                    cl = Color.Black;

                return cl;
            }
            else
            {
                return Color.Cyan;
            }
        }

        public static Color GetConstraintTimeColor(DateTime date)
        {
            var result = Color.Black;

            var hours = (date - DateTime.UtcNow).TotalHours;

            if (hours > 24)
            {
                result = Color.FromHex("#929292"); //gris
            }
            else if (hours > 12)
            {
                result = Color.FromHex("#00a8ff"); //blue
            }
            else if (hours > 6)
            {
                result = Color.FromHex("#14c649"); //green
            }
            else if (hours > 3)
            {
                result = Color.FromHex("#ffba00"); //yellow
            }
            else if (hours > 1)
            {
                result = Color.FromHex("#f98409"); //orange
            }
            else if (hours > 0)
            {
                result = Color.FromHex("#d0092f"); //red
            }
            else if (hours <= 0)
            {
                result = Color.FromHex("#850af8"); //purple
            }

            return result;
        }
    }
}
