﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMCargoOrCharter : BaseViewModel
    {
        public VMAircraft AircraftCurrent { get; set; }
        public VMAirport AirportFrom { get; set; }
        public VMAirport AirportTo { get; set; }
        public VMAirport AirportCurrent { get; set; }
        public string LocationCurrent { get; set; }
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Description { get; set; }
        public string CargoOrPAXValue { get; set; }
        public string Heading { get; set; }
        public string Distance { get; set; }
        public bool ConstraintTime { get; set; }
        public Color ConstraintTimeColor { get; set; }
        public string ConstraintTimeDisplay { get; set; }
        public bool IsCargo { get; set; }

        public bool ShowCargos { get; set; }
        public bool ShowPAXs { get; set; }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { _isSelected = value; OnPropertyChanged(nameof(IsSelected)); }
        }

        public VMCargoOrCharter(WSServerMission.Cargo c)
        {
            IsCargo = true;
            ShowCargos = true;
            CargoOrPAXValue = string.Format("{0:N0} lbs", c.Weight);
            Heading = string.Format("{0:0}°", c.Heading);
            Distance = string.Format("{0:0}NM", c.Distance);

            LocationFrom = c.DepartureAirport.ICAO;
            LocationTo = c.DestinationAirport.ICAO;

            Description = c.Description;

            ConstraintTime = c.CargoAvailableTime.HasValue;
            if (ConstraintTime)
            {
                ConstraintTimeColor = VMMission.GetConstraintTimeColor(c.CargoAvailableTime.Value);
                ConstraintTimeDisplay = Strings.AVAILABLE_AT(c.CargoAvailableTime.Value);
            }

            if (c.CurrentAirport != null)
            {
                AirportCurrent = new VMAirport(c.CurrentAirport);
                if (c.CurrentAirport.ICAO == c.DestinationAirport.ICAO)
                {
                    LocationCurrent = Strings.MISSION_DELIVERED;
                }
                else
                {
                    LocationCurrent = c.CurrentAirport.ICAO;
                }
                Latitude = c.CurrentAirport.Latitude;
                Longitude = c.CurrentAirport.Longitude;
            }
            else if (c.CurrentAircraft != null)
            {
                AircraftCurrent = new VMAircraft(c.CurrentAircraft);
                if (c.CurrentAircraft.AircraftStatus == AircraftStatus.InFlight)
                {
                    LocationCurrent = c.CurrentAircraft.Identifier + " flying";
                }
                else if (c.CurrentAircraft.CurrentAirport != null)
                {
                    LocationCurrent = c.CurrentAircraft.Identifier + " at " + c.CurrentAircraft.CurrentAirport.ICAO;
                }
                else
                {
                    LocationCurrent = c.CurrentAircraft.Identifier;
                }
                Latitude = c.CurrentAircraft.Latitude;
                Longitude = c.CurrentAircraft.Longitude;
            }

            AirportFrom = new VMAirport(c.DepartureAirport);
            AirportTo = new VMAirport(c.DestinationAirport);
        }

        public VMCargoOrCharter(WSServerMission.Charter c)
        {
            ShowPAXs = true;
            CargoOrPAXValue = string.Format("{0:N0} PAX", c.PassengersNumber);
            Heading = string.Format("{0:0}°", c.Heading);
            Distance = string.Format("{0:0}NM", c.Distance);

            LocationFrom = c.DepartureAirport.ICAO;
            LocationTo = c.DestinationAirport.ICAO;

            Description = c.Description;

            ConstraintTime = c.CharterAvailableTime.HasValue;
            if (ConstraintTime)
            {
                ConstraintTimeColor = VMMission.GetConstraintTimeColor(c.CharterAvailableTime.Value);
                ConstraintTimeDisplay = Strings.AVAILABLE_AT(c.CharterAvailableTime.Value);
            }

            if (c.CurrentAirport != null)
            {
                AirportCurrent = new VMAirport(c.CurrentAirport);
                if (c.CurrentAirport.ICAO == c.DestinationAirport.ICAO)
                {
                    LocationCurrent = Strings.MISSION_DELIVERED;
                }
                else
                {
                    LocationCurrent = c.CurrentAirport.ICAO;
                }
                Latitude = c.CurrentAirport.Latitude;
                Longitude = c.CurrentAirport.Longitude;
            }
            else if (c.CurrentAircraft != null)
            {
                AircraftCurrent = new VMAircraft(c.CurrentAircraft);
                if (c.CurrentAircraft.AircraftStatus == AircraftStatus.InFlight)
                {
                    LocationCurrent = c.CurrentAircraft.Identifier + " flying";
                }
                else if (c.CurrentAircraft.CurrentAirport != null)
                {
                    LocationCurrent = c.CurrentAircraft.Identifier + " at " + c.CurrentAircraft.CurrentAirport.ICAO;
                }
                else
                {
                    LocationCurrent = c.CurrentAircraft.Identifier;
                }
                Latitude = c.CurrentAircraft.Latitude;
                Longitude = c.CurrentAircraft.Longitude;
            }

            AirportFrom = new VMAirport(c.DepartureAirport);
            AirportTo = new VMAirport(c.DestinationAirport);
        }
    }
}
