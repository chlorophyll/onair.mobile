﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMAirport : BaseViewModel
    {
        public string ICAO { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public VMAirport(WSServerMission.Airport a)
        {
            ICAO = a.ICAO;
            Name = a.DisplayName;
            Size = a.Size;
            Latitude = a.Latitude;
            Longitude = a.Longitude;
        }
    }
}
