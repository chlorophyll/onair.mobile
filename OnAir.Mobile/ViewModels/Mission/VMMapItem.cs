﻿using Ecommunity.Mobile;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    public class VMMapItem : BaseViewModel
    {
        public string Title { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public ImageSource Icon { get; set; }
        public string Location { get; set; }

        public MapItemType Type { get; set; }

        public VMMapItem()
        {

        }
    }

    public enum MapItemType
    {
        Cargo,
        Charter,
        Airport
    }
}
