﻿using Ecommunity.Mobile;
using System;
using System.Collections.Generic;
using System.Text;
using OnAir.SharedBusiness;
using System.Threading;
using Xamarin.Forms;
using System.Linq;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMMissions : BaseViewModel
    {
        public static Guid CompanyId;

        private bool _enableTabs;
        public bool EnableTabs
        {
            get { return _enableTabs; }
            set { _enableTabs = value; OnPropertyChanged(nameof(EnableTabs)); }
        }

        private bool _showLoader;
        public bool ShowLoader
        {
            get { return _showLoader; }
            set { _showLoader = value; OnPropertyChanged(nameof(ShowLoader)); }
        }

        private bool _showStatus;
        public bool ShowStatus
        {
            get { return _showStatus; }
            set { _showStatus = value; OnPropertyChanged(nameof(ShowStatus)); }
        }

        private string _status;
        public string Status
        {
            get { return _status; }
            set { _status = value; OnPropertyChanged(nameof(Status)); }
        }

        private bool _showSearchTab;
        public bool ShowSearchTab
        {
            get { return _showSearchTab; }
            set { _showSearchTab = value; OnPropertyChanged(nameof(ShowSearchTab)); }
        }
        
        private VMMissionTab _selectedTab;
        public VMMissionTab SelectedTab
        {
            get { return _selectedTab; }
            set { if (_selectedTab == value) return; _selectedTab = value; OnPropertyChanged(nameof(SelectedTab)); }
        }

        private string _icao;
        public string ICAO
        {
            get { return _icao; }
            set { _icao = value; OnPropertyChanged(nameof(ICAO)); }
        }

        private bool _searchAround;
        public bool SearchAround
        {
            get { return _searchAround; }
            set { _searchAround = value; OnPropertyChanged(nameof(SearchAround)); }
        }

        private bool _canSearchMissions;
        public bool CanSearchMissions
        {
            get { return _canSearchMissions; }
            set { _canSearchMissions = value; OnPropertyChanged(nameof(CanSearchMissions)); }
        }

        private VMComboBoxItem _selectedRangeArea;
        public VMComboBoxItem SelectedRangeArea
        {
            get { return _selectedRangeArea; }
            set { _selectedRangeArea = value; OnPropertyChanged(nameof(SelectedRangeArea)); }
        }

        private VMComboBoxItem _selectedDirection;
        public VMComboBoxItem SelectedDirection
        {
            get { return _selectedDirection; }
            set { _selectedDirection = value; OnPropertyChanged(nameof(SelectedDirection)); }
        }

        private VMOrderTab _selectedFilterTab;
        public VMOrderTab SelectedOrderTab
        {
            get { return _selectedFilterTab; }
            set { _selectedFilterTab = value; OnPropertyChanged(nameof(SelectedOrderTab)); }
        }

        public ObservableRangeCollection<VMMissionTab> Tabs { get; set; }
        public ObservableRangeCollection<VMOrderTab> OrderTabs { get; set; }

        public ObservableRangeCollection<VMMission> Missions { get; set; }

        public ObservableRangeCollection<VMComboBoxItem> RangeAreas { get; set; }

        public ObservableRangeCollection<VMComboBoxItem> Directions { get; set; }


        public Ecommunity.Mobile.Action SearchExpandAction { get; set; }
        public Ecommunity.Mobile.Action SearchMissionsAction { get; set; }

        private VMMissionTab _tabSearch;
        private VMMissionTab _tabPending;

        private List<VMMission> _searchMissions;

        private bool _searchIsExpanded = true;
        private double _searchExpandedHeight = -1;

        public VMMissions()
        {
            Tabs = new ObservableRangeCollection<VMMissionTab>();
            OrderTabs = new ObservableRangeCollection<VMOrderTab>();
            Missions = new ObservableRangeCollection<VMMission>();
            RangeAreas = new ObservableRangeCollection<VMComboBoxItem>();
            Directions = new ObservableRangeCollection<VMComboBoxItem>();

            RangeAreas.AddRange(VMComboBoxItem.GetRangeAreas());
            Directions.AddRange(VMComboBoxItem.GetDirections());

            SelectedRangeArea = RangeAreas.First(a => a.Value == 2);
            SelectedDirection = Directions.First();

            SearchMissionsAction = new Ecommunity.Mobile.Action((context, control) => {
                SearchMissions();
            });

            SearchExpandAction = new Ecommunity.Mobile.Action((context, control) => {
                SearchExpand();
            });

            _tabSearch = new VMMissionTab()
            {
                Name = "Search"
            };

            _tabPending = new VMMissionTab()
            {
                Name = "Pending"
            };

            Tabs.Add(_tabSearch);
            Tabs.Add(_tabPending);


            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.BaseAirport,
                Name = "Airport"
            });

            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.Distance,
                Name = "Distance"
            });

            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.ExpireDate,
                Name = "Expires"
            });

            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.Heading,
                Name = "Heading"
            });

            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.PAX,
                Name = "PAX"
            });

            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.Penalty,
                Name = "Penalty"
            });

            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.Price,
                Name = "Price"
            });

            OrderTabs.Add(new VMOrderTab()
            {
                Type = OrderTabEnum.Weight,
                Name = "Weight"
            });

            SelectedOrderTab = OrderTabs.First();

            Load();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(SelectedTab))
            {
                ShowSearchTab = false;

                if (SelectedTab == _tabPending)
                {
                    LoadPendingMissions();
                }
                else if (SelectedTab == _tabSearch)
                {
                    ShowSearchTab = true;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Missions.Clear();

                        if (_searchMissions != null)
                        {
                            Missions.AddRange(OrderMissions(_searchMissions));
                            CheckMissionsEmpty();
                        }
                    });
                }
            }
            else if (propertyName == nameof(ICAO))
            {
                if (ICAO != null)
                {
                    if (ICAO.Length > 5)
                    {
                        ICAO = ICAO.Substring(0, 5);
                    }
                    CanSearchMissions = ICAO.Length >= 3;
                }
                else
                {
                    CanSearchMissions = false;
                }
            }
            else if (propertyName == nameof(SelectedOrderTab))
            {
                if (Missions != null && Missions.Count > 0)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        var missions = Missions.ToList();
                        Missions.Clear();
                        Missions.AddRange(OrderMissions(missions));
                        CheckMissionsEmpty();
                    });
                }
            }
            else if (propertyName == nameof(ShowLoader))
            {
                EnableTabs = !ShowLoader;
            }
        }

        private void Load()
        {
            ShowLoader = true;

            var t = new Thread(() =>
            {
                try
                {
                    CompanyId = WS.GetCompanyInfos(new WSGetCompanyInfos2Params() 
                    { 
                        WorldId = VMHome.Instance.SelectedWorld.Id
                    }).Id;
                }
                catch (Exception ex)
                {
                    ShowErrorAndGoBack(ex);
                }

                ShowLoader = false;
                SelectedTab = _tabPending;
            });

            t.Start();
        }

        private void LoadPendingMissions()
        {
            ShowLoader = true;
            ShowStatus = false;

            Device.BeginInvokeOnMainThread(() =>
            {
                Missions.Clear();
            });

            var t = new Thread(() =>
            {
                try
                {
                    var missions = WS.GetPendingMissions(new WSServerGetPendingMissionsParam()
                    {
                        CompanyId = CompanyId
                    });

                    if (missions != null)
                    {
                        var vmMissions = missions.Select(m => new VMMission(m)).ToList();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Missions.AddRange(OrderMissions(vmMissions));
                            CheckMissionsEmpty();
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Status = Strings.MISSIONS_EMPTY_LIST;
                            ShowStatus = true;
                        });
                    }
                }
                catch (Exception ex)
                {
                    ShowErrorAndGoBack(ex);
                }

                ShowLoader = false;
            });

            t.Start();
        }

        private void SearchMissions()
        {
            ShowStatus = false;
            ShowLoader = true;

            CollapseSearch();

            Device.BeginInvokeOnMainThread(() =>
            {
                Missions.Clear();
            });

            var t = new Thread(() =>
            {
                try
                {
                    var searchMissionsResult = WS.SearchMissions(new WSServerSearchMissionsParam()
                    {
                        CompanyId = CompanyId,
                        BaseAirportICAO = ICAO,
                        RangeAreaIndex = SelectedRangeArea.Value,
                        Direction = (MissionDirectionShared)SelectedDirection.Value,
                        SearchAround = SearchAround
                    });

                    if (searchMissionsResult != null)
                    {
                        _searchMissions = searchMissionsResult.Select(m => new VMMission(m)).ToList();
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            Missions.AddRange(OrderMissions(_searchMissions));
                            CheckMissionsEmpty();
                        });
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            ShowStatus = true;
                            Status = Strings.MISSIONS_SEARCH_EMPTY_LIST;
                        });
                    }
                }
                catch (Exception ex)
                {
                    ShowErrorAndGoBack(ex);
                }

                ShowLoader = false;
            });

            t.Start();
        }

        private void SearchExpand()
        {
            if (_searchIsExpanded)
            {
                CollapseSearch();
            }
            else
            {
                ExpandSearch();
            }
        }

        private void CollapseSearch()
        {
            if (_searchIsExpanded == false)
            {
                return;
            }

            var uc = EcommunityPage.FindControlById(Ids.phone320568.Missions.ControlUserControlSearchId).GetView<AbsoluteLayout>();
            uc.IsClippedToBounds = true;

            var start = AbsoluteLayout.GetLayoutBounds(uc).Height;
            if (_searchExpandedHeight == -1)
            {
                _searchExpandedHeight = start;
            }

            var anim = new Animation(h =>
            {
                var bounds = AbsoluteLayout.GetLayoutBounds(uc);
                bounds.Height = h;
                AbsoluteLayout.SetLayoutBounds(uc, bounds);
            }, start,  0);

            _searchIsExpanded = false;

            anim.Commit(uc, "HeightAnim", 16, 500, Easing.Linear);
        }

        private void ExpandSearch()
        {
            if (_searchIsExpanded)
            {
                return;
            }

            var uc = EcommunityPage.FindControlById(Ids.phone320568.Missions.ControlUserControlSearchId).GetView<AbsoluteLayout>();
            uc.IsClippedToBounds = true;

            var start = AbsoluteLayout.GetLayoutBounds(uc).Height;

            var anim = new Animation(h =>
            {
                var bounds = AbsoluteLayout.GetLayoutBounds(uc);
                bounds.Height = h;
                AbsoluteLayout.SetLayoutBounds(uc, bounds);
            }, start, _searchExpandedHeight);

            _searchIsExpanded = true;

            anim.Commit(uc, "HeightAnim", 16, 500, Easing.Linear);
        }

        private void ShowErrorAndGoBack(Exception ex)
        {
            Services.Logger.Debug("EX : " + ex.Message + " " + ex.StackTrace);
            Device.BeginInvokeOnMainThread(() =>
            {
                EcommunityMobile.MainPage.DisplayAlert(Strings.MISSIONS_FAIL_ALERT_TITLE, Strings.MISSIONS_FAIL_ALERT_MESSAGE, Strings.OK);
                EcommunityMobile.MainPage.PopAsync();
            });
        }

        private void CheckMissionsEmpty()
        {
            if (Missions.Count == 0)
            {
                Status = Strings.MISSIONS_EMPTY_LIST;
                ShowStatus = true;
            }
            else
            {
                Status = null;
                ShowStatus = false;
            }
        }

        private IEnumerable<VMMission> OrderMissions(IEnumerable<VMMission> missions)
        {
            IEnumerable<VMMission> result = null;

            switch (SelectedOrderTab.Type)
            {
                case OrderTabEnum.Weight:
                    result = missions.OrderByDescending(m => m.GetModel().Cargos.Sum(c => c.Weight));
                    break;
                case OrderTabEnum.PAX:
                    result = missions.OrderByDescending(m => m.GetModel().Charters.Sum(c => c.PassengersNumber));
                    break;
                case OrderTabEnum.Price:
                    result = missions.OrderByDescending(m => m.GetModel().Pay);
                    break;
                case OrderTabEnum.Heading:
                    result = missions.OrderBy(m => m.GetModel().MainAirportHeading);
                    break;
                case OrderTabEnum.Distance:
                    result = missions.OrderBy(m => m.GetModel().TotalDistance);
                    break;
                case OrderTabEnum.ExpireDate:
                    result = missions.OrderBy(m => m.GetModel().ExpirationDate);
                    break;
                case OrderTabEnum.Penalty:
                    result = missions.OrderBy(m => m.GetModel().Penality);
                    break;
                case OrderTabEnum.BaseAirport:
                    result = missions.OrderBy(m => m.GetModel().MainAirport?.ICAO);
                    break;
            }

            return result;
        }
    }

    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMMissionTab : BaseViewModel
    {
        public string Name { get; set; }
    }


    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMOrderTab : BaseViewModel
    {
        public string Name { get; set; }
        public OrderTabEnum Type { get; set; }
        public bool Desc { get; set; }
    }

    public enum OrderTabEnum
    {
        Weight,
        PAX,
        Price, 
        Heading,
        Distance,
        ExpireDate,
        Penalty,
        BaseAirport
    }
}
