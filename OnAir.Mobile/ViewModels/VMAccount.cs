﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    [Ecommunity.Mobile.Attributes.PageViewModel]
    public class VMAccount : BaseViewModel
    {
        private bool _showLoader;
        public bool ShowLoader
        {
            get { return _showLoader; }
            set { _showLoader = value; OnPropertyChanged(nameof(ShowLoader)); }
        }

        private string _accountName;
        public string AccountName
        {
            get { return _accountName; }
            set { _accountName = value; OnPropertyChanged(nameof(AccountName)); }
        }

        private string _accountEmail;
        public string AccountEmail
        {
            get { return _accountEmail; }
            set { _accountEmail = value; OnPropertyChanged(nameof(AccountEmail)); }
        }

        private string _companyName;
        public string CompanyName
        {
            get { return _companyName; }
            set { _companyName = value; OnPropertyChanged(nameof(CompanyName)); }
        }

        private string _companyCode;
        public string CompanyCode
        {
            get { return _companyCode; }
            set { _companyCode = value; OnPropertyChanged(nameof(CompanyCode)); }
        }

        private WSGetWorldsResult.World _selectedWorld;
        public WSGetWorldsResult.World SelectedWorld
        {
            get { return _selectedWorld; }
            set { _selectedWorld = value; OnPropertyChanged(nameof(SelectedWorld)); }
        }

        public ObservableRangeCollection<WSGetWorldsResult.World> Worlds { get; set; }
        public Ecommunity.Mobile.Action LogoutAction { get; set; }

        public VMAccount()
        {
            Worlds = new ObservableRangeCollection<WSGetWorldsResult.World>();
            LogoutAction = new Ecommunity.Mobile.Action((control, context) => { Logout(); });

            Worlds.AddRange(VMHome.Instance.Worlds);
            _selectedWorld = Worlds.First(o => o.Id == VMHome.Instance.SelectedWorld.Id);

            Load();
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(SelectedWorld) && SelectedWorld != null)
            {
                VMHome.Instance.SelectedWorld = VMHome.Instance.Worlds.First(o => o.Id == SelectedWorld.Id);
                Load();
            }
        }

        private void Logout()
        {
            BUSAuth.Logout();
            EcommunityMobile.MainPage.PopAsync();
        }

        private void Load()
        {
            ShowLoader = true;

            var t = new Thread(() => 
            {
                try
                {
                    var userInfos = WSAuth.GetUserInfo();
                    AccountName = userInfos.FirstName + " " + userInfos.LastName;
                    AccountEmail = userInfos.Email;

                    try
                    {
                        var companyInfos = WS.GetCompanyInfos(new WSGetCompanyInfos2Params()
                        {
                            WorldId = SelectedWorld.Id
                        });

                        CompanyCode = companyInfos.AirlineCode;
                        CompanyName = companyInfos.Name;
                    }
                    catch (Exception)
                    {
                        CompanyCode = "";
                        CompanyName = "";

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            EcommunityMobile.MainPage.DisplayAlert(Strings.ACCOUNT_FAIL_ALERT_TITLE, "No company available in this world", Strings.OK);
                        });
                    }
                }
                catch (Exception)
                {
                    Device.BeginInvokeOnMainThread(() => 
                    {
                        EcommunityMobile.MainPage.DisplayAlert(Strings.ACCOUNT_FAIL_ALERT_TITLE, Strings.ACCOUNT_FAIL_ALERT_MESSAGE, Strings.OK);
                        EcommunityMobile.MainPage.PopAsync();
                    });
                }

                ShowLoader = false;
            });

            t.Start();
        }
    }

    public class VMWorld
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
    }
}
