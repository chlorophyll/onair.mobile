﻿using Ecommunity.Mobile;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    public class App : Application
    {
        public App()
        {
            var mainPage = new NavigationPage();
            MainPage = mainPage;

            EcommunityMobile.Init(mainPage);
            EcommunitySettings.ApplicationId = new Guid("028d948b-28c3-461e-8411-3bf614c34b81");
            EcommunitySettings.Channel = Ecommunity.Mobile.Channel.Stable;
            EcommunitySettings.StableSettings.ForceUpdate = true;
            EcommunitySettings.StableSettings.Version = 4;

            i18n.SetCurrentLanguage("en");

            //Ecommunity.Mobile.Services.Analytics.Init("");

            EcommunityMobile.RegisterPageCustomizer(new Ecommunity.Mobile.HomeMenuCloserCustomizer());
            EcommunityMobile.RegisterPageCustomizer(Ids.phone320568.OnAir.Id, new ListViewRemoveSelectionPageCustomizer(Ids.phone320568.OnAir.ControlV2ListViewId));
            EcommunityMobile.RegisterPageCustomizer(Ids.phone320568.Notifications.Id, new ListViewRemoveSelectionPageCustomizer(Ids.phone320568.Notifications.ControlV2ListViewId));
            EcommunityMobile.RegisterPageCustomizer(Ids.phone320568.Notifications.Id, new ListViewRemoveSelectionPageCustomizer(Ids.phone320568.Notifications.ControlV2ListViewId));
            EcommunityMobile.RegisterPageCustomizer(Ids.phone320568.MissionTabDetail.Id, new ListViewRemoveSelectionPageCustomizer(Ids.phone320568.MissionTabDetail.ControlV2ListViewcloneId));

            Services.RegisterService(typeof(INavigationService), new OnAirNavigationService());
            Services.RegisterService(typeof(IViewModelActivator), new OnAirViewModelActivator());

            EcommunitySettings.StartupPageSettings.RawJSONFilePath = "OnAir.Mobile.startup.json";
            EcommunitySettings.StartupPageSettings.RawJSONFileAssemblyType = typeof(App);
            EcommunitySettings.StartupPageSettings.StartupPageBackground = "splashscreen.png";
            EcommunitySettings.StartupPageSettings.StatusColor = Color.White;
            EcommunitySettings.StartupPageSettings.LoaderColor = Color.White;

            var startupPage = new Ecommunity.Mobile.StartupPage();
            mainPage.Navigation.PushAsync(startupPage);
        }
    }
}
