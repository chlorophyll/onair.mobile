﻿using Ecommunity.Mobile;
using Newtonsoft.Json;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace OnAir.Mobile
{
    public static class WS
    {
        public const string SCHEME = "https";

        public static Guid WORLD_CUMULUS_ID = new Guid("ad3ec8a4-246e-4abb-84a9-9dbc43bb6ae6");
        public static Guid WORLD_STRATUS_ID = new Guid("be6ab20f-809f-4c57-aaa6-9e78a3022ba8");
        public static Guid WORLD_THUNDER_ID = new Guid("c83eb5d5-9dc5-452f-b261-69b45cb0951b");

        public const string HOSTNAME_CUMULUS = "cumulus.onair.company";
        public const string HOSTNAME_STRATUS = "stratus.onair.company";
        public const string HOSTNAME_THUNDER = "thunder.onair.company";

        public const string PATH = "api";

        public static string GetUrl(string path)
        {
            string hostname = null;

            if (BUSConf.Conf.SelectedWorldId == WORLD_STRATUS_ID)
            {
                hostname = HOSTNAME_STRATUS;
            }
            else if (BUSConf.Conf.SelectedWorldId == WORLD_THUNDER_ID)
            {
                hostname = HOSTNAME_THUNDER;
            }
            else
            {
                hostname = HOSTNAME_CUMULUS;
            }

            return string.Format("{0}://{1}/{2}/{3}", SCHEME, hostname, PATH, path);
        }

        public static WSResult TakeMission(WSServerTakeMissionParam p)
        {
            p.AccessToken = BUSAuth.AccessToken;
            return Post<WSResult>(GetUrl("TakeMission"), p);
        }
        public static WSGetNotificationResult GetNotifications(WSGetNotificationsParam p)
        {
            p.AccessToken = BUSAuth.AccessToken;
            return Post<WSGetNotificationResult>(GetUrl("GetNotifications"), p);
        }

        public static List<WSServerMission> GetPendingMissions(WSServerGetPendingMissionsParam p)
        {
            p.AccessToken = BUSAuth.AccessToken;
            return Post<List<WSServerMission>>(GetUrl("GetPendingMissions"), p);
        }

        public static List<WSServerMission> SearchMissions(WSServerSearchMissionsParam p)
        {
            p.AccessToken = BUSAuth.AccessToken;
            return Post<List<WSServerMission>>(GetUrl("SearchMissions"), p);
        }

        public static WSServerGetCompanyInfosResult GetCompanyInfos(WSGetCompanyInfos2Params p)
        {
            p.AccessToken = BUSAuth.AccessToken;
            return Post<WSServerGetCompanyInfosResult>(GetUrl("GetCompanyInfos2"), p);
        }

        public static WSGetWorldsResult GetWorlds(WSParam p)
        {
            p.AccessToken = BUSAuth.AccessToken;
            return Post<WSGetWorldsResult>(GetUrl("GetWorlds"), p);
        }

        public static WSLauncherGetNewsResult GetNews()
        {
            return Post<WSLauncherGetNewsResult>("https://www.onair.company/api/GetNews", null);
        }

        private static T Post<T>(string url, object p)
        {
            var result = default(T);

            var json = JsonConvert.SerializeObject(p);

            Services.Logger.Debug("REQUESTING " + url);
            Services.Logger.Debug("\t " + json);

            using (var client = new HttpClient())
            {
                var response = client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json")).Result;

                response.EnsureSuccessStatusCode();

                var resultJson = response.Content.ReadAsStringAsync().Result;

                result = JsonConvert.DeserializeObject<T>(resultJson);
                Services.Logger.Debug("REQUESTED " + url);
            }

            return result;
        }
    }
}
