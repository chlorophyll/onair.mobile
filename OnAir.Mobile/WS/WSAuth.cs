﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace OnAir.Mobile
{
    public static class WSAuth
    {
        public static UserInfos GetUserInfo()
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("onair", BUSAuth.AccessToken);
                var response = client.GetAsync("https://auth.onair.company/api/users/me").Result;

                response.EnsureSuccessStatusCode();

                var resultjson = response.Content.ReadAsStringAsync().Result;
                var result = JsonConvert.DeserializeObject<UserInfos>(resultjson);
                return result;
            }
        }


        public static string Login(LoginModel p)
        {
            var json = JsonConvert.SerializeObject(p);
            using (var client = new HttpClient())
            {
                var response = client.PostAsync("https://auth.onair.company/api/login", new StringContent(json, Encoding.UTF8, "application/json")).Result;

                response.EnsureSuccessStatusCode();

                var token = response.Content.ReadAsStringAsync().Result;
                return token;
            }
        }

        public class LoginModel
        {
            public Guid DirectoryId { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
        }

        public class UserInfos
        {
            public Guid Id { get; set; }
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public Guid DirectoryId { get; set; }
            public List<string> Roles { get; set; }
        }
    }
}
