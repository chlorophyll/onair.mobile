﻿using Ecommunity.Mobile;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OnAir.Mobile
{
    public class Conf
    {
        public static Conf Current;

        public Guid DirectoryId { get; set; }

        public static void Load()
        {
            var json = Services.FileIOSync.ReadAllText(Path.Combine(Files.AppResourcesDirectory, "conf.json"));
            Current = JsonConvert.DeserializeObject<Conf>(json);
        }
    }
}
