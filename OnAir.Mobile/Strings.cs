﻿using Ecommunity.Mobile;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    public static class Strings
    {

        public static string OK { get { return "OK".Translate(); } }
        public static string CANCEL { get { return "CANCEL".Translate(); } }

        public static string DATE_FORMAT_1 { get { return "DATE_FORMAT_1".Translate(); } }
        public static string DATE_FORMAT_2 { get { return "DATE_FORMAT_2".Translate(); } }

        public static string MISSION_INFO_TITLE { get { return "MISSION_INFO_TITLE".Translate(); } }
        public static string MISSION_DELIVERED { get { return "MISSION_DELIVERED".Translate(); } }
        public static string MISSION_TAKE_FAIL_ALERT_TITLE { get { return "MISSION_TAKE_FAIL_ALERT_TITLE".Translate(); } }
        public static string MISSION_TAKE_FAIL_ALERT_MESSAGE { get { return "MISSION_TAKE_FAIL_ALERT_MESSAGE".Translate(); } }
        public static string MISSION_TAKE_SUCCESS_ALERT_TITLE { get { return "MISSION_TAKE_SUCCESS_ALERT_TITLE".Translate(); } }
        public static string MISSION_TAKE_SUCCESS_ALERT_MESSAGE { get { return "MISSION_TAKE_SUCCESS_ALERT_MESSAGE".Translate(); } }

        public static string MISSIONS_FAIL_ALERT_TITLE { get { return "MISSIONS_FAIL_ALERT_TITLE".Translate(); } }
        public static string MISSIONS_FAIL_ALERT_MESSAGE { get { return "MISSIONS_FAIL_ALERT_MESSAGE".Translate(); } }
        public static string MISSIONS_EMPTY_LIST { get { return "MISSIONS_EMPTY_LIST".Translate(); } }
        public static string MISSIONS_SEARCH_EMPTY_LIST { get { return "MISSIONS_SEARCH_EMPTY_LIST".Translate(); } }

        public static string ACCOUNT_FAIL_ALERT_MESSAGE { get { return "ACCOUNT_FAIL_ALERT_MESSAGE".Translate(); } }
        public static string ACCOUNT_FAIL_ALERT_TITLE { get { return "ACCOUNT_FAIL_ALERT_TITLE".Translate(); } }

        public static string AUTH_LOGING { get { return "AUTH_LOGING".Translate(); } }
        public static string AUTH_LOGING_SUCCESS { get { return "AUTH_LOGING_SUCCESS".Translate(); } }
        public static string AUTH_LOGING_FAIL { get { return "AUTH_LOGING_FAIL".Translate(); } }
        public static string HOME_LOADING { get { return "HOME_LOADING".Translate(); } }
        public static string HOME_LOADING_FAIL { get { return "HOME_LOADING_FAIL".Translate(); } }

        public static string AUTH_ALERT_TITLE { get { return "AUTH_ALERT_TITLE".Translate(); } }
        public static string AUTH_ALERT_MESSAGE { get { return "AUTH_ALERT_MESSAGE".Translate(); } }

        public static string AVAILABLE_AT (DateTime date)
        {
            var dateFormat = DATE_FORMAT_1;
            return string.Format("AVAILABLE_AT_{0}".Translate(), date.ToString(dateFormat));
        }
    }
}
