﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ecommunity.Mobile;
using Ecommunity.Mobile.Components;
using Xamarin.Forms;

namespace OnAir.Mobile
{
    public class ListViewRemoveSelectionPageCustomizer : IPageCustomizer
    {
        private Guid _listControlId;

        public ListViewRemoveSelectionPageCustomizer(Guid listControlId)
        {
            _listControlId = listControlId;
        }

        public void CustomizePage(Ecommunity.Mobile.Components.Page componentsPage, Ecommunity.Mobile.Page ecommunityPage, Xamarin.Forms.Page xamarinPage)
        {
            var listView = ecommunityPage.FindControlById(_listControlId) as V2ListView;
            var customListView = listView.GetView<V2CustomListView>();

            customListView.SelectionMode = ListViewSelectionMode.None;
            //customListView.RemoveSelection = true;
        }
    }
}