﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OnAir.Mobile
{
    public static class BUSFormat
    {
        public static string Amount(double amount)
        {
            return string.Format(new CultureInfo("en-us"), "{0:N0}", amount);
        }
    }
}
