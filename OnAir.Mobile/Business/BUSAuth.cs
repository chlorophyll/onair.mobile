﻿using Ecommunity.Mobile;
using OnAir.SharedBusiness;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    public static class BUSAuth
    {
        public const string FILE_TOKEN = "token.txt";

        public static string AccessToken = null;

        public static async void GoToLoginPage(Guid redirectPageId, bool alert)
        {
            if (alert)
            {
                var auth = await EcommunityMobile.MainPage.DisplayAlert(Strings.AUTH_ALERT_TITLE, Strings.AUTH_ALERT_MESSAGE, Strings.OK, Strings.CANCEL);

                if (auth)
                {
                    EcommunityMobile.Navigate(Ids.phone320568.SignInScrollViewContainer.Id, new VMAuth(redirectPageId));
                }
            }
            else
            {
                EcommunityMobile.Navigate(Ids.phone320568.SignInScrollViewContainer.Id, new VMAuth(redirectPageId));
            }
        }

        public static void Logout()
        {
            if (Services.FileIOSync.FileExists(FILE_TOKEN))
            {
                Services.FileIOSync.DeleteFile(FILE_TOKEN);
            }
            AccessToken = null;
            VMHome.Instance.LoadNewsAndNotifs();
        }

        public static void Login(string email, string password)
        {
            var token = WSAuth.Login(new WSAuth.LoginModel()
            {
                DirectoryId = Conf.Current.DirectoryId,
                Email = email,
                Password = password
            });

            AccessToken = token;
            SaveAccessToken(token);
            VMHome.Instance.LoadNewsAndNotifs();
        }

        public static bool IsLoggedIn()
        {
            return AccessToken != null;
        }

        public static void SaveAccessToken(string token)
        {
            Services.FileIOSync.WriteAllText(FILE_TOKEN, token);
        }

        public static void LoadAccessToken()
        {
            if (Services.FileIOSync.FileExists(FILE_TOKEN))
            {
                AccessToken = Services.FileIOSync.ReadAllText(FILE_TOKEN);
            }
        }
    }
}
