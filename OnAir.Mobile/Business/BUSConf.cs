﻿using Ecommunity.Mobile;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    public static class BUSConf
    {
        public const string FILE_CONF = "conf.json";

        public static LocalConf Conf { get; set; }

        public static void LoadConf()
        {
            if (!Services.FileIOSync.FileExists(FILE_CONF))
            {
                Conf = new LocalConf();
                SaveConf();
            }

            var json = Services.FileIOSync.ReadAllText(FILE_CONF);
            Conf = JsonConvert.DeserializeObject<LocalConf>(json);
        }

        public static void SaveConf()
        {
            var json = JsonConvert.SerializeObject(Conf);
            Services.FileIOSync.WriteAllText(FILE_CONF, json);
        }
    }

    public class LocalConf
    {
        public Guid SelectedWorldId { get; set; }
    }
}
