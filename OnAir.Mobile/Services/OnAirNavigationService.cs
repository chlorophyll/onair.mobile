﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    public class OnAirNavigationService : Ecommunity.Mobile.EcommunityNavigationService
    {
        public override void Navigate(Guid pageId, object context = null, bool replaceCurrentScreen = false)
        {
            var shouldNav = false;

            if (pageId == Ids.phone320568.Missions.Id || pageId == Ids.phone320568.MyAccount.Id)
            {
                if (BUSAuth.IsLoggedIn())
                {
                    shouldNav = true;
                }
                else
                {
                    var alert = pageId == Ids.phone320568.Missions.Id;
                    BUSAuth.GoToLoginPage(pageId, alert);
                }
            }
            else
            {
                shouldNav = true;
            }

            if (shouldNav)
            {
                base.Navigate(pageId, context, replaceCurrentScreen);
            }
        }
    }
}
