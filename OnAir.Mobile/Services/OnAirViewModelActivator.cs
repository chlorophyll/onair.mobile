﻿using Ecommunity.Mobile;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.Mobile
{
    public class OnAirViewModelActivator : Ecommunity.Mobile.EcommunityViewModelActivator
    {
        public override object InstanciateViewModel(object context, string viewModelType)
        {
            if (viewModelType != null && viewModelType.Contains("OnAir.Mobile.VMHome"))
            {
                return VMHome.Instance;
            }
            return base.InstanciateViewModel(context, viewModelType);
        }
    }
}
