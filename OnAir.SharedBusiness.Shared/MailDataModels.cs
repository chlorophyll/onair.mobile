﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public class MailHireData
    {
        public decimal HourlySalary { get; set; }
        public decimal MonthlySalary { get; set; }
    }

    public class MailFreelanceData
    {
        public Guid CompanyId { get; set; }
        public decimal TotalWages { get; set; }
        public List<Guid> CargoIds { get; set; }
        public List<Guid> CharterIds { get; set; }
        public Guid? AssignedWorkId { get; set; }
        public string Comment { get; set; }
        public DateTime ExpirationDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public MailFreelanceDataStatus Status { get; set; }
    }

    public enum MailFreelanceDataStatus
    {
        None = 0,
        Accepted = 1,
        Decline = 2
    }

    public class MailAircraftSellData
    {
        public Guid AircraftId { get; set; }
        public decimal Amount { get; set; }
    }

    public class MailAircraftRentData
    {
        public Guid AircraftId { get; set; }
        public decimal AmountPerDay { get; set; }
        public DateTime StartDate { get; set; }
        public int Days { get; set; }
    }

    public class MailMoneyData
    {
        public decimal Amount { get; set; }
    }
}
