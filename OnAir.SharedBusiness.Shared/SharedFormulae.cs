﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public class SharedFormulae
    {
        public static double FuelLbsToGallons(int fuelType, double fuelLbs)
        {
            var fuelTypeLbs = (fuelType == 0 ? 6 : 6.7);
            return fuelLbs / fuelTypeLbs;
        }

        public static double FuelLbsToGallons(FuelTypeEnum fuelType, double fuelLbs)
        {
            return FuelLbsToGallons((int)fuelType, fuelLbs);
        }

        public static double FuelGallonsToLbs(int fuelType, double fuelGallons)
        {
            var fuelTypeLbs = (fuelType == 0 ? 6 : 6.7);
            return fuelGallons * fuelTypeLbs;
        }

        public static double FuelGallonsToLbs(FuelTypeEnum fuelType, double fuelGallons)
        {
            return FuelGallonsToLbs((int)fuelType, fuelGallons);
        }

        public static double ComputeHeading(double lat1, double lon1, double lat2, double lon2)
        {
            double lat1Rad = DegreeToRadian(lat1);
            double lat2Rad = DegreeToRadian(lat2);
            double lon1Rad = DegreeToRadian(-lon1);
            double lon2Rad = DegreeToRadian(-lon2);

            double d = 2 * Math.Asin(Math.Sqrt(Math.Pow((Math.Sin((lat1Rad - lat2Rad) / 2)), 2) +
                Math.Cos(lat1Rad) * Math.Cos(lat2Rad) * Math.Pow((Math.Sin((lon1Rad - lon2Rad) / 2)), 2)));

            double tc =
                (
                    Math.Atan2(
                        Math.Sin(lon1Rad - lon2Rad) * Math.Cos(lat2Rad),
                        Math.Cos(lat1Rad) * Math.Sin(lat2Rad) -
                        Math.Sin(lat1Rad) * Math.Cos(lat2Rad) * Math.Cos(lon1Rad - lon2Rad)
                              )
                 ) % (2 * Math.PI);
            var trueHeading = RadianToDegree(tc);
            if (trueHeading <= 0)
                trueHeading = trueHeading + 360;

            return trueHeading;            
        }

        public static double ComputeDistance(double lat1, double lon1, double lat2, double lon2)
        {
            double lat1Rad = DegreeToRadian(lat1);
            double lat2Rad = DegreeToRadian(lat2);
            double lon1Rad = DegreeToRadian(-lon1);
            double lon2Rad = DegreeToRadian(-lon2);

            double d = 2 * Math.Asin(Math.Sqrt(Math.Pow((Math.Sin((lat1Rad - lat2Rad) / 2)), 2) +
                Math.Cos(lat1Rad) * Math.Cos(lat2Rad) * Math.Pow((Math.Sin((lon1Rad - lon2Rad) / 2)), 2)));


            var distance = DistanceToNM(d);
            return distance;
        }

        static public string TimeSpanToReadableString(System.TimeSpan span)
        {
            string formatted;
            if (span > new System.TimeSpan(1, 0, 0, 0))
                formatted = string.Format("{0}{1}",
                   span.Duration().Days > 0 ? string.Format("{0:0} day{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
                   span.Duration().Hours > 0 ? string.Format("{0:0} hour{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty);
            else
                formatted = string.Format("{0}{1}{2}",
                   span.Duration().Days > 0 ? string.Format("{0:0} day{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
                   span.Duration().Hours > 0 ? string.Format("{0:0} hour{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
                   span.Duration().Minutes > 0 ? string.Format("{0:0} minute{1}, ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty);
            //span.Duration().Seconds > 0 ? string.Format("{0:0} second{1}", span.Seconds, span.Seconds == 1 ? String.Empty : "s") : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 seconds";

            return formatted;
        }

        static private double mod(double y, double x)
        {
            double md = y - x * (int)(y / x);
            if (md < 0)
                md = md + x;
            return md;

        }
        static public double DistanceToRadian(double distNM)
        {
            return (Math.PI / (180.0 * 60.0)) * distNM;
        }

        public static void ComputePointOnGreatCircle(double lat1, double lon1, double trueHeading, double distance, ref double lat2, ref double lon2)
        {
            double lat1Rad = DegreeToRadian(lat1);
            double lon1Rad = DegreeToRadian(-lon1);
            double tc = DegreeToRadian(trueHeading);
            double d = DistanceToRadian(distance);

            double lat2Rad = Math.Asin(Math.Sin(lat1Rad) * Math.Cos(d) + Math.Cos(lat1Rad) * Math.Sin(d) * Math.Cos(tc));
            
            double dlon = Math.Atan2(Math.Sin(tc) * Math.Sin(d) * Math.Cos(lat1Rad), Math.Cos(d) - Math.Sin(lat1Rad) * Math.Sin(lat2Rad));
            double lon2Rad = mod(lon1Rad - dlon + Math.PI, 2 * Math.PI) - Math.PI;
            
            lat2 = RadianToDegree(lat2Rad);
            lon2 = -RadianToDegree(lon2Rad);
            if (lon2 < -180)
                lon2 += 360;
            if (lon2 >= 180)
                lon2 -= 360;

        }    
        
        public static double FeetToNM(double feet)
        {
            return feet / 6076.115486;
        }

        private static double DegreeToRadian(double angleDegree)
        {
            return ((Math.PI / 180.0) * angleDegree);
        }

        private static double DistanceToNM(double distRadian)
        {
            return ((180.0 * 60.0) / Math.PI) * distRadian;
        }


        private static double RadianToDegree(double angleradian)
        {
            double angle = angleradian * 180.0 / Math.PI;
            return angle;
        }
    }
}
