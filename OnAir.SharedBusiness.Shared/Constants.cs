﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public static class Constants
    {
        public const  int FLIGHT_DUTY_MAX_HOURS = 14;
        public const int MAX_FLIGHT_HOURS = 9;
        public const int REST_HOURS = 8;
        public const int RELOCATE_BASE_HOURS = 24*2;
        public static readonly Guid TEMP_NEW_AIRCRAFT_ID = new Guid("0d593b59-4604-4686-a58c-478988aae813");
        public const double FUEL_CONTAINER_GALS = 5.3;

        public static readonly Guid CARGO_TYPE_FUEL_CONTAINER_100LL_ID = new Guid("3c359961-cc1b-4f16-bc60-349d1a109b1c");
        public static readonly Guid CARGO_TYPE_FUEL_CONTAINER_JET_ID = new Guid("7ba892aa-aa2e-40d5-bf38-f7d894dd8059");
    }


    public enum FuelTypeEnum
    {
        Fuel100LL = 0,
        FuelJET = 1
    }

    public enum CompanySkillEnum
    {
        FINANCE_Main_100,
        FINANCE_Loans_110,
        FINANCE_ParkingFee_111,
        FINANCE_WeeklyOwnership_120,
        FINANCE_LandingFee_121,
        FBO_MAIN_200,
        FBO_Ateliers_210,
        FBO_Mec_220,
        FBO_FuelResell_230,
        FBO_WorkshopResell_240,
        MISSION_Main_300,
        MISSION_LoadSpeedPAX_310,
        MISSION_LoadSpeedFuel_311,
        MISSION_LoadSpeedCargos_312,
        MISSION_LastMinute_320,
        MISSION_Medical_330,
        MISSION_Dangerous_340,
        AI_Main_400,
        AI_TrainingEfficient_410,
        AI_TrainingSpeed_411,
        AI_Condition_420,
        AI_LessFuel_421,
        AI_TransportSpeed_422,
        AI_MoreExp_430
    }
}
