﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public class WSLauncherIsUnderMaintenanceResult : WSResult
    {
        public bool IsUnderMaintenance { get; set; }
    }
}
