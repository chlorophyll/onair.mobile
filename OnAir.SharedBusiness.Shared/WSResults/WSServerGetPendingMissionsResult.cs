﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSServerGetPendingMissionsResult : WSResult
    {
        public WSServerMission[] Missions { get; set; }
    }

    public class WSServerMission
    {
        public Guid Id { get; set; }
        public Cargo[] Cargos { get; set; }
        public Charter[] Charters { get; set; }
        public Missiontype MissionType { get; set; }
        public Airport MainAirport { get; set; }       
        public float TotalDistance { get; set; }
        public float MainAirportHeading { get; set; }
        public string Description { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public float Pay { get; set; }
        public float Penality { get; set; }
        public float ReputationImpact { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? TakenDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? PaidDate { get; set; }
        public float TotalCargoTransported { get; set; }
        public int TotalPaxTransported { get; set; }
        public int Category { get; set; }
        public int State { get; set; }
        public int XP { get; set; }
        public int SkillPoint { get; set; }       
        public float RealPay { get; set; }
        public float LastMinuteBonus { get; set; }
        public bool CanAccess { get; set; }
        public string CanAccessDisplay { get; set; }
        public bool IsLastMinute { get; set; }
        public bool IsFavorited { get; set; }

        public class Missiontype
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public string ShortName { get; set; }
            public string Description { get; set; }
            public float BaseReputationImpact { get; set; }
            public float BasePay { get; set; }
            public float BasePenality { get; set; }
        }

        public class Cargo
        {
            public Guid Id { get; set; }
            public string CargoTypeId { get; set; }
            public Cargotype CargoType { get; set; }
            public Airport CurrentAirport { get; set; }
            public float Weight { get; set; }
            public Airport DepartureAirport { get; set; }
            public Airport DestinationAirport { get; set; }     
            public float Distance { get; set; }
            public float Heading { get; set; }
            public DateTime? CargoAvailableTime { get; set; }         
            public string Description { get; set; }
            public bool HumanOnly { get; set; }
            public Aicraft CurrentAircraft { get; set; }
        }

        public class Cargotype
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int CargoTypeCategory { get; set; }
        }

        public class Aicraft
        {
            public Guid Id { get; set; }
            public string Identifier { get; set; }
            public AircraftStatus AircraftStatus { get; set; }
            public Airport CurrentAirport { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
        }

        public class Airport
        {
            public Guid Id { get; set; }
            public string ICAO { get; set; }
            public float TimeOffsetInSec { get; set; }
            public float LocalTimeOpenInHoursSinceMidnight { get; set; }
            public float LocalTimeCloseInHoursSinceMidnight { get; set; }
            public string IATA { get; set; }
            public string Name { get; set; }
            public string State { get; set; }
            public string CountryCode { get; set; }
            public string CountryName { get; set; }
            public string City { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public float Elevation { get; set; }
            public bool HasLandRunway { get; set; }
            public bool HasWaterRunway { get; set; }
            public bool HasHelipad { get; set; }
            public int Size { get; set; }
            public int TransitionAltitude { get; set; }
            public DateTime LastMissionGeneration { get; set; }
            public DateTime? LastMETARDate { get; set; }
            public string LastMETARRaw { get; set; }     
            public string DisplayName { get; set; }
            public float UTCTimeOpenInHoursSinceMidnight { get; set; }
            public float UTCTimeCloseInHoursSinceMidnight { get; set; }
        }
        public class Charter
        {
            public Guid Id { get; set; }
        
            public Chartertype CharterType { get; set; }
            public Airport CurrentAirport { get; set; }
            public int PassengersNumber { get; set; }
            public Airport DepartureAirport { get; set; }
            public Airport DestinationAirport { get; set; }
            public float Distance { get; set; }
            public float Heading { get; set; }
            public DateTime? CharterAvailableTime { get; set; }          
            public string Description { get; set; }     
            public bool HumanOnly { get; set; }
            public Aicraft CurrentAircraft { get; set; }
        }

        public class Chartertype
        {
            public Guid Id { get; set; }
            public string Name { get; set; }
            public int CharterTypeCategory { get; set; }
        }
    }
}
