﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSGetWorldsResult : WSResult
    {
        public List<World> Worlds { get; set; }

        public class World
        {
            public string Name { get; set; }
            public Guid Id { get; set; }
        }
    }
}
