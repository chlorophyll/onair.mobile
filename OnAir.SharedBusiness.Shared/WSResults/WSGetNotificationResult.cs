﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSGetNotificationResult : WSResult
    {
        public WSNotification[] Notifications { get; set; }
    }

    public class WSNotification
    {
        public bool IsRead { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
    }
}
