﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public class WSLauncherGetNewsResult : WSResult
    {
        public List<News> PublishedNews { get; set; }

        public class News
        {
            public Guid Id { get; set; }
            public DateTime PublishDate { get; set; }
            public string ImageUrl { get; set; }
            public string Title { get; set; }
            public string Permalink { get; set; }
            public string VideoUrl { get; set; }
            public string Categorie { get; set; }
            public string Auteur { get; set; }
            public string Subtitle { get; set; }
            public string Chapo { get; set; }
            public string Content { get; set; }
            public DateTime? UnpublishDate { get; set; }
            public bool IsActive { get; set; }
        }
    }
}
