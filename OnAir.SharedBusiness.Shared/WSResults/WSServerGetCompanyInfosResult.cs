﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSServerGetCompanyInfosResult : WSResult
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string AirlineCode { get; set; }
        public DateTime? LastConnection { get; set; }
        public DateTime LastReportDate { get; set; }
        public Guid OwnerId { get; set; }
        public double Reputation { get; set; }
        public bool RealTime { get; set; }
        public bool AllowUserTP { get; set; }
        public DateTime CreationDate { get; set; }
        public int DifficultyLevel { get; set; }
        public bool NoSimulator { get; set; }
        public double UTCOffsetinHours { get; set; }
    }
}
