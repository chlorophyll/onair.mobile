﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public class WSResult
    {
        public bool IsSuccess { get; set; }
        public string Error { get; set; }
    }
}
