﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public static class CompanySkillsSharedBusiness
    {
        public static List<CompanySkillInfo> GetSkillsInfos(List<CompanySkillEnum> acquiredSkills)
        {
            var result = new List<CompanySkillInfo>();

            result.Add(GetInfoFINANCE_Main_100(acquiredSkills));
            result.Add(GetInfoFINANCE_Loans_110(acquiredSkills));
            result.Add(GetInfoFINANCE_ParkingFee_111(acquiredSkills));
            result.Add(GetInfoFINANCE_WeeklyOwnership_120(acquiredSkills));
            result.Add(GetInfoFINANCE_LandingFee_121(acquiredSkills));

            result.Add(GetInfoFBO_MAIN_200(acquiredSkills));
            result.Add(GetInfoFBO_Ateliers_210(acquiredSkills));
            result.Add(GetInfoFBO_Mec_220(acquiredSkills));
            result.Add(GetInfoFBO_FuelResell_230(acquiredSkills));
            result.Add(GetInfoFBO_WorkshopResell_240(acquiredSkills));

            result.Add(GetInfoMISSION_Main_300(acquiredSkills));
            result.Add(GetInfoMISSION_LoadSpeedPAX_310(acquiredSkills));
            result.Add(GetInfoMISSION_LoadSpeedFuel_311(acquiredSkills));
            result.Add(GetInfoMISSION_LoadSpeedCargos_312(acquiredSkills));
            result.Add(GetInfoMISSION_LastMinute_320(acquiredSkills));
            result.Add(GetInfoMISSION_Medical_330(acquiredSkills));
            result.Add(GetInfoMISSION_Dangerous_340(acquiredSkills));

            result.Add(GetInfoAI_Main_400(acquiredSkills));
            result.Add(GetInfoAI_TrainingEfficient_410(acquiredSkills));
            result.Add(GetInfoAI_TrainingSpeed_411(acquiredSkills));
            result.Add(GetInfoAI_Condition_420(acquiredSkills));
            result.Add(GetInfoAI_LessFuel_421(acquiredSkills));
            result.Add(GetInfoAI_TransportSpeed_422(acquiredSkills));
            result.Add(GetInfoAI_MoreExp_430(acquiredSkills));

            return result;
        }

        private static CompanySkillInfo GetInfoFINANCE_Main_100(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FINANCE_Main_100,
                Title = "Bank Loans Agreement",
                BackgroundImage = "bg-orange.png",
                MaxBulles = 1,
                BulleCost = 1,
                CanAccess = true,
                Bulle1Desc = "Unlock loans at the bank"
            };
        }

        private static CompanySkillInfo GetInfoFINANCE_Loans_110(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FINANCE_Loans_110,
                Title = "Better Interest Rates",
                BackgroundImage = "bg-orange.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FINANCE_Main_100),
                Bulle1Desc = "The interest rate of your loans are 1% better",
                Bulle2Desc = "The interest rate of your loans are 2% better",
                Bulle3Desc = "The interest rate of your loans are 3% better"
            };
        }

        private static CompanySkillInfo GetInfoFINANCE_ParkingFee_111(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FINANCE_ParkingFee_111,
                Title = "Negociate Parking Fees",
                BackgroundImage = "bg-orange.png",
                MaxBulles = 2,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FINANCE_Main_100),
                Bulle1Desc = "Parking fee reduced by 5%",
                Bulle2Desc = "Parking fee reduced by 10%"
            };
        }

        private static CompanySkillInfo GetInfoFINANCE_WeeklyOwnership_120(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FINANCE_WeeklyOwnership_120,
                Title = "Negociate Aircrafts ownership",
                BackgroundImage = "bg-orange.png",
                MaxBulles = 3,
                BulleCost = 3,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FINANCE_Loans_110),
                Bulle1Desc = "Aircrafts ownership reduced by 3%",
                Bulle2Desc = "Aircrafts ownership reduced by 5%",
                Bulle3Desc = "Aircrafts ownership reduced by 10%"
            };
        }

        private static CompanySkillInfo GetInfoFINANCE_LandingFee_121(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FINANCE_LandingFee_121,
                Title = "Negociate Landing Fees",
                BackgroundImage = "bg-orange.png",
                MaxBulles = 2,
                BulleCost = 3,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FINANCE_ParkingFee_111),
                Bulle1Desc = "Landing fee reduced by 5%",
                Bulle2Desc = "Landing fee reduced by 10%"
            };
        }

        private static CompanySkillInfo GetInfoFBO_MAIN_200(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FBO_MAIN_200,
                Title = "FBOs Agreement",
                BackgroundImage = "bg-vert.png",
                MaxBulles = 3,
                BulleCost = 1,
                CanAccess = true,
                Bulle1Desc = "You can build FBOs and get a Logistic Query license for one sector (delivers every 5 days)",
                Bulle2Desc = "Get a second Logistic Query license (delivers every 2 days)",
                Bulle3Desc = "Get a third Logistic Query license (delivers every day)"
            };
        }

        private static CompanySkillInfo GetInfoFBO_Ateliers_210(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FBO_Ateliers_210,
                Title = "Workshops Agreement",
                BackgroundImage = "bg-vert.png",
                MaxBulles = 2,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FBO_MAIN_200),
                Bulle1Desc = "Unlocks building of level 1 workhops in your FBOs",
                Bulle2Desc = "Unlocks building of level 2 workhops in your FBOs"
            };
        }

        private static CompanySkillInfo GetInfoFBO_Mec_220(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FBO_Mec_220,
                Title = "Mecanos Training",
                BackgroundImage = "bg-vert.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FBO_Ateliers_210),
                Bulle1Desc = "Your Mechanics are working 5% faster",
                Bulle2Desc = "Your Mechanics are working 10% faster",
                Bulle3Desc = "Your Mechanics are working 20% faster"
            };
        }

        private static CompanySkillInfo GetInfoFBO_FuelResell_230(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FBO_FuelResell_230,
                Title = "Fuel Seller Agreement",
                BackgroundImage = "bg-vert.png",
                MaxBulles = 1,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FBO_MAIN_200),
                Bulle1Desc = "You can sell fuel to other players"
            };
        }

        private static CompanySkillInfo GetInfoFBO_WorkshopResell_240(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.FBO_WorkshopResell_240,
                Title = "Workhop Service Selling",
                BackgroundImage = "bg-vert.png",
                MaxBulles = 1,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.FBO_Ateliers_210),
                Bulle1Desc = "Your workshop can accept other players aircrafts"
            };
        }

        private static CompanySkillInfo GetInfoMISSION_Main_300(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.MISSION_Main_300,
                Title = "Passengers Licence",
                BackgroundImage = "bg-bleu.png",
                MaxBulles = 1,
                BulleCost = 1,
                CanAccess = true,
                Bulle1Desc = "Unlocks Jobs with passengers"
            };
        }

        private static CompanySkillInfo GetInfoMISSION_LoadSpeedPAX_310(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.MISSION_LoadSpeedPAX_310,
                Title = "Boarding training",
                BackgroundImage = "bg-bleu.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.MISSION_Main_300),
                Bulle1Desc = "Passengers are boarding and deboarding 5% faster",
                Bulle2Desc = "Passengers are boarding and deboarding 10% faster",
                Bulle3Desc = "Passengers are boarding and deboarding 20% faster"
            };
        }

        private static CompanySkillInfo GetInfoMISSION_LoadSpeedFuel_311(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.MISSION_LoadSpeedFuel_311,
                Title = "Fuel loading speed",
                BackgroundImage = "bg-bleu.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.MISSION_Main_300),
                Bulle1Desc = "Fuel is loaded 5% faster",
                Bulle2Desc = "Fuel is loaded 10% faster",
                Bulle3Desc = "Fuel is loaded 20% faster"
            };
        }

        private static CompanySkillInfo GetInfoMISSION_LoadSpeedCargos_312(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.MISSION_LoadSpeedCargos_312,
                Title = "Cargo loading speed",
                BackgroundImage = "bg-bleu.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.MISSION_Main_300),
                Bulle1Desc = "Cargos are loaded 5% faster",
                Bulle2Desc = "Cargos are loaded 10% faster",
                Bulle3Desc = "Cargos are loaded 20% faster"
            };
        }

        private static CompanySkillInfo GetInfoMISSION_LastMinute_320(List<CompanySkillEnum> acquiredSkills)
        {
            var canAccess = acquiredSkills.Contains(CompanySkillEnum.MISSION_LoadSpeedFuel_311);
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.MISSION_LastMinute_320,
                Title = "Last Minute Jobs",
                BackgroundImage = "bg-bleu.png",
                MaxBulles = 1,
                BulleCost = 2,
                CanAccess = canAccess,
                Bulle1Desc = "You can accept Last Minute Jobs"
            };
        }

        private static CompanySkillInfo GetInfoMISSION_Medical_330(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.MISSION_Medical_330,
                Title = "Medical Jobs",
                BackgroundImage = "bg-bleu.png",
                MaxBulles = 1,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.MISSION_LastMinute_320),
                Bulle1Desc = "You company is certified for Medical Transportation"
            };
        }

        private static CompanySkillInfo GetInfoMISSION_Dangerous_340(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.MISSION_Dangerous_340,
                Title = "Dangerous Jobs",
                BackgroundImage = "bg-bleu.png",
                MaxBulles = 1,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.MISSION_LoadSpeedCargos_312),
                Bulle1Desc = "You company is certified for Dangerous Material Transportation"
            };
        }

        private static CompanySkillInfo GetInfoAI_Main_400(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Title = "Hiring agreement",
                BackgroundImage = "bg-violet.png",
                Skill = CompanySkillEnum.AI_Main_400,
                MaxBulles = 1,
                BulleCost = 1,
                CanAccess = true,
                Bulle1Desc = "You can hire Employees and Freelances"
            };
        }

        private static CompanySkillInfo GetInfoAI_TrainingEfficient_410(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.AI_TrainingEfficient_410,
                Title = "Trainings Facilities",
                BackgroundImage = "bg-violet.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.AI_Main_400),
                Bulle1Desc = "Your employees certifications and skill trainings are 5% more efficient",
                Bulle2Desc = "Your employees certifications and skill trainings are 10% more efficient",
                Bulle3Desc = "Your employees certifications and skill trainings are 20% more efficient"
            };
        }

        private static CompanySkillInfo GetInfoAI_TrainingSpeed_411(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.AI_TrainingSpeed_411,
                Title = "Better instructors",
                BackgroundImage = "bg-violet.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.AI_Main_400),
                Bulle1Desc = "Your employees trainings are 5% faster",
                Bulle2Desc = "Your employees trainings are 10% faster",
                Bulle3Desc = "Your employees trainings are 20% faster"
            };
        }

        private static CompanySkillInfo GetInfoAI_Condition_420(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.AI_Condition_420,
                Title = "Aircraft use",
                BackgroundImage = "bg-violet.png",
                MaxBulles = 3,
                BulleCost = 1,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.AI_TrainingEfficient_410),
                Bulle1Desc = "Your pilots will handle the aircrafts more carefully (1%)",
                Bulle2Desc = "Your pilots will handle the aircrafts more carefully (2%)",
                Bulle3Desc = "Your pilots will handle the aircrafts more carefully (3%)"
            };
        }

        private static CompanySkillInfo GetInfoAI_LessFuel_421(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.AI_LessFuel_421,
                Title = "Fuel use",
                BackgroundImage = "bg-violet.png",
                MaxBulles = 3,
                BulleCost = 3,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.AI_TrainingEfficient_410),
                Bulle1Desc = "Your pilots will use 1% less fuel",
                Bulle2Desc = "Your pilots will use 2% less fuel",
                Bulle3Desc = "Your pilots will use 4% less fuel"
            };
        }

        private static CompanySkillInfo GetInfoAI_TransportSpeed_422(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.AI_TransportSpeed_422,
                Title = "Better transportations",
                BackgroundImage = "bg-violet.png",
                MaxBulles = 3,
                BulleCost = 3,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.AI_TrainingSpeed_411),
                Bulle1Desc = "Your employees with be transported 5% faster",
                Bulle2Desc = "Your employees with be transported 10% faster",
                Bulle3Desc = "Your employees with be transported 20% faster"
            };
        }

        private static CompanySkillInfo GetInfoAI_MoreExp_430(List<CompanySkillEnum> acquiredSkills)
        {
            return new CompanySkillInfo()
            {
                Skill = CompanySkillEnum.AI_MoreExp_430,
                Title = "Better Processes",
                BackgroundImage = "bg-violet.png",
                MaxBulles = 3,
                BulleCost = 2,
                CanAccess = acquiredSkills.Contains(CompanySkillEnum.AI_Condition_420),
                Bulle1Desc = "Your employees will acquire new skills 1% faster",
                Bulle2Desc = "Your employees will acquire new skills 2% faster",
                Bulle3Desc = "Your employees will acquire new skills 3% faster"
            };
        }
    }

    public class CompanySkillInfo
    {
        public CompanySkillEnum Skill { get; set; }
        public int MaxBulles { get; set; }
        public int BulleCost { get; set; }
        public bool CanAccess { get; set; }
        public string Bulle1Desc { get; set; }
        public string Bulle2Desc { get; set; }
        public string Bulle3Desc { get; set; }
        public string BackgroundImage { get; set; }
        public string Title { get; set; }
    }
}
