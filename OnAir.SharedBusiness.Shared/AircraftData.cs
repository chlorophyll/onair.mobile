﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public enum AircraftStatus
    {
        Idle,
        Maintenance,
        ApronWork,
        InFlight,
        Warp
    };

    public enum InFlightState
    {
        Taxi,
        Departure,
        Cruise,
        Approach,
        FinalApproach
    }
    /*
        Database statuses : 
             Idle => Avion dispo, posé à un terrain
             Maintenance => Avion pas dispo en cours de maintenance, posé à un terrain
             Repair => Avion pas dispo en cours de réparation, posé à un terrain

        World statuses :
            ApronWork => Avion pas dispo en cours de : chargement cargos/pax/fuel, déchargemeent cargo/pax/fuel, posé à un terrain
                    A RAJOUTER : agents pour : clean, icing, cattering
                    
            Taxy => Avion pas dispo en cours de roulage (départ ou arrivée)
            Fly => Avion pas dispo, il vole
     */
}
