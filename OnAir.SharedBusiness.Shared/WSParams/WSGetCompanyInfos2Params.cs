﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSGetCompanyInfos2Params : WSParam
    {
        public Guid WorldId { get; set; }
    }
}
