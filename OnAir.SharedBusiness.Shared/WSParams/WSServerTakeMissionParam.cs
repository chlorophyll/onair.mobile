﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSServerTakeMissionParam : WSParam
    {
        public Guid MissionId { get; set; }
        public Guid CompanyId { get; set; }
    }
}
