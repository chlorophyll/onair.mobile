﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSServerSearchMissionsParam : WSParam
    {
        public Guid CompanyId { get; set; }
        public int RangeAreaIndex { get; set; }
        public bool SearchAround { get; set; }
        public string BaseAirportICAO { get; set; }
        public MissionDirectionShared Direction { get; set; }
    }

    public enum MissionDirectionShared
    {
        From,
        To,
        //Both
    }
}
