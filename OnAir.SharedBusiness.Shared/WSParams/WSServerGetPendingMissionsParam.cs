﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public class WSServerGetPendingMissionsParam : WSParam
    {
        public Guid CompanyId { get; set; }
    }
}
