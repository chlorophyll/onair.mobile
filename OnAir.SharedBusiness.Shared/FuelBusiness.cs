﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public static class FuelBusiness
    {
        public static double GetFuelWeight(double fuelGallons, int engineType)
        {
            double fuelLBSWeight = (engineType == 0 ? 6 : 6.7);
            return fuelGallons * fuelLBSWeight;
        }
    }
}
