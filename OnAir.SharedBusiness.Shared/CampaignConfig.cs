﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{


    public class ScenarioConfig
    {
        public Guid? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string InitialPilotAirportICAO { get; set; }
        public string PilotName { get; set; }
        public decimal InitialMoney { get; set; }
        public int DifficultyLevel { get; set; }
        public bool AllowUserTP { get; set; }
        public double InitialReputation { get; set; }
        public int Version { get; set; }
    }
}
