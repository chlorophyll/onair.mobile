﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public class OnAirTCPClient : TCPClient
    {
        //public const string TCP_SERVER_IP = "192.168.0.10";  //ALIENWARE Fouchy
        //public const string TCP_SERVER_IP = "192.168.0.46";  //Arthur Maison
        //public const string TCP_SERVER_IP = "192.168.0.10";  //ALIENWARE Fouchy
        //public const string TCP_SERVER_IP = "192.168.0.16";  //Arthur Fouchy
        //public const string TCP_SERVER_IP = "192.168.0.19";  //MSI Fouchy
        //public const string TCP_SERVER_IP = "192.168.1.20";  //Arthur chez Perrine
        //public const string TCP_SERVER_IP = "35.205.195.142"; //Prod by IP pas utilisé
        //public const string TCP_SERVER_IP = "192.168.43.79"; // Arthur tél

#if DEV
        public const string TCP_SERVER_IP_LOCAL = "192.168.1.251";
        public const string TCP_SERVER_IP_EXTERN = "86.243.114.226";
#elif DEVFOUCHY
        public const string TCP_SERVER_IP_LOCAL = "192.168.1.251";
        public const string TCP_SERVER_IP_EXTERN = "192.168.1.251";
#elif LOCAL
        public const string TCP_SERVER_IP_LOCAL = "192.168.0.44";
        public const string TCP_SERVER_IP_EXTERN = "192.168.0.44";
#elif LOCALANTOINE // avec sql et serveur d authentification sur le serveur DEV de Fouchy 192.168.1.26
        public const string TCP_SERVER_IP_LOCAL = "192.168.1.25";
        public const string TCP_SERVER_IP_EXTERN = "192.168.1.25";
#else
        public const string TCP_SERVER_IP_LOCAL = "world.onair.company"; //PROD VM
        public const string TCP_SERVER_IP_EXTERN = "world.onair.company"; //PROD VM
#endif

        public event EventHandler<byte[]> BroadcastReceived;

        public const int TCP_SERVER_PORT = 11000;

        public void ConnectServer(bool listenAsync)
        {
            Connect(TCP_SERVER_IP_LOCAL, TCP_SERVER_PORT, listenAsync);
        }

        public void ConnectClient(bool listenAsync)
        {
            Connect(TCP_SERVER_IP_EXTERN, TCP_SERVER_PORT, listenAsync);
        }

        public void BroadcastEmployees(BroadcastEmployeesParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            SendCommand(CommandType.BroadcastEmployees, bytes);
        }

        public void BroadcastAircraft(BroadcastAircraftParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            SendCommand(CommandType.BroadcastAircraft, bytes);
        }

        public void AddWarpAgentSync(AddWarpAgentParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            SendCommand(CommandType.AddWarpAgent, bytes);
        }

        public void BroadcastPopupSync(BroadcastPopup parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            SendCommand(CommandType.BroadcastPopup, bytes);
        }

        public void PushSync(PushParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            SendCommand(CommandType.Push, bytes);
        }

        public GetServerStatusResult GetServerStatusSync(GetServerStatusParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = SendCommand(CommandType.GetServerStatus, bytes);
            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<GetServerStatusResult>(resultJson);
            return result;
        }

        public async Task<GetServerStatusResult> GetServerStatusAsync(GetServerStatusParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = await SendCommandAsync(CommandType.GetServerStatus, bytes);
            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<GetServerStatusResult>(resultJson);
            return result;
        }

        public GetConnectedCompaniesResult GetConnectedCompanies(GetConnectedCompaniesParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = SendCommand(CommandType.GetConnectedCompanies, bytes);
            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<GetConnectedCompaniesResult>(resultJson);
            return result;
        }

        public async Task<LoginResult> LoginAsync(LoginParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = await SendCommandAsync(CommandType.Login, bytes);
            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<LoginResult>(resultJson);
            return result;
        }


        public async Task<GetAircraftAgentsResult> GetAircraftAgents(GetAircraftAgentsParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = await SendCommandAsync(CommandType.GetAircraftAgents, bytes);
            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<GetAircraftAgentsResult>(resultJson);
            return result;
        }

        public GetAircraftAgentsResult GetAircraftAgentsSync(GetAircraftAgentsParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = SendCommand(CommandType.GetAircraftAgents, bytes);
            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<GetAircraftAgentsResult>(resultJson);
            return result;
        }

        public void RefreshAircraftAfterSellSync(RefreshAircraftAfterSellParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            SendCommand(CommandType.RefreshAircraftAfterSell, bytes);          
        }

        public async Task<AddAircraftAgentResult> AddAircraftAgent(AddAircraftAgentParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = await SendCommandAsync(CommandType.AddAircraftAgent, bytes);

            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<AddAircraftAgentResult>(resultJson);
            return result;
        }

        public AddAircraftAgentResult AddAircraftAgentSync(AddAircraftAgentParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            var resultBytes = SendCommand(CommandType.AddAircraftAgent, bytes);

            var resultJson = Encoding.UTF8.GetString(resultBytes);

            var result = JsonConvert.DeserializeObject<AddAircraftAgentResult>(resultJson);
            return result;
        }

        public async Task PauseAircraftAsync(PauseAircraftParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            await SendCommandAsync(CommandType.PauseAircraft, bytes);
        }

        public async Task LaunchAircraftAsync(LaunchAircraftParams2 parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            await SendCommandAsync(CommandType.LaunchAircraft, bytes);
        }

        public void LaunchAircraftSync(LaunchAircraftParams2 parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);

            SendCommand(CommandType.LaunchAircraft, bytes);
        }

        //public async Task<GetCompanyAircraftsResult> GetCompanyAircrafts(GetCompanyAircraftsParams parameters)
        //{
        //    var json = JsonConvert.SerializeObject(parameters);
        //    var bytes = Encoding.UTF8.GetBytes(json);

        //    var resultBytes = await SendCommandAsync(CommandType.GetCompanyAircrafts, bytes);

        //    var resultJson = Encoding.UTF8.GetString(resultBytes);

        //    var result = JsonConvert.DeserializeObject<GetCompanyAircraftsResult>(resultJson);
        //    return result;
        //}

        public GetCompanyAircraftsResult GetCompanyAircraftsSync(GetCompanyAircraftsParams parameters)
        {
            var aircrafts = new List<GetCompanyAircraftsResult.WorldAircraft>();

            var index = 0;
            var number = 5;

            GetCompanyAircraftsResult result = null;

            do
            {
                parameters.Index = index;
                parameters.NumberOfAircrafts = number;

                var json = JsonConvert.SerializeObject(parameters);
                var bytes = Encoding.UTF8.GetBytes(json);

                var resultBytes = SendCommand(CommandType.GetCompanyAircrafts, bytes);

                var resultJson = Encoding.UTF8.GetString(resultBytes);
                result = JsonConvert.DeserializeObject<GetCompanyAircraftsResult>(resultJson);
                aircrafts.AddRange(result.WorldAircrafts);

                index += number;
            }
            while (result.IsEnd == false);

            result.WorldAircrafts = aircrafts;
            return result;
        }

        public async Task SaveWorldAsync()
        {
            await SendCommandAsync(CommandType.SaveWorld, new byte[12]);
        }

        public void RemoveAircraft(RemoveAircraftParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            SendCommand(CommandType.RemoveAircraft, bytes);
        }

        public async Task AddPlayerAircraft(AddPlayerAircraftParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            await SendCommandAsync(CommandType.AddPlayerAircraft, bytes);
        }

        public void AddPlayerAircraftSync(AddPlayerAircraftParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            SendCommand(CommandType.AddPlayerAircraft, bytes);
        }

        public void UpdatePlayerAircraftFromSimSync(UpdatePlayerAircraftFromSimParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            SendCommand(CommandType.UpdatePlayerAircraftValuesFromSim, bytes);
        }

        public async Task UpdatePlayerAircraftFromSim(UpdatePlayerAircraftFromSimParams parameters)
        {
            var json = JsonConvert.SerializeObject(parameters);
            var bytes = Encoding.UTF8.GetBytes(json);
            await SendCommandAsync(CommandType.UpdatePlayerAircraftValuesFromSim, bytes);
        }

        protected override void OnMessageReceived(Guid messageId, byte[] bytes)
        {
            if (messageId == TCPParams.BROADCAST_ID)
            {
                BroadcastReceived?.Invoke(this, bytes);
            }
        }
    }
}
