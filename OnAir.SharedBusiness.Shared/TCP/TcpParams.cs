﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public static class TCPParams
    {
        public static readonly byte[] ACK = Encoding.UTF8.GetBytes("ACK");
        public static readonly byte[] NACK = Encoding.UTF8.GetBytes("NACK");
        public static readonly Guid BROADCAST_ID = new Guid("9a4f992e-f400-4091-95fd-15b41ffc68ff");
    }

    public enum BroadcastType
    {
        UpdateAircraft = 0,
        UpdateEmployees = 1,
        Push = 2,
        AutoDisconnect = 3,
        AutoKill = 4,
        UpdateCargosAndCharters = 5,
        Popup
    }

    public enum CommandType
    {
        LaunchAircraft = 1,
        GetCompanyAircrafts = 2,
        SaveWorld = 3,
        RemoveAircraft = 4,
        PauseAircraft = 7,
        AddAircraftAgent = 8,
        GetAircraftAgents = 9,
        Login = 10,
        GetServerStatus = 11,
        AddPlayerAircraft = 12,
        UpdatePlayerAircraftValuesFromSim = 13,
        RefreshAircraftAfterSell = 14,
        Push = 15,
        GetConnectedCompanies = 16,
        AddWarpAgent = 17,
        BroadcastAircraft = 18,
        BroadcastEmployees = 20,
        BroadcastPopup = 21
    }

    public enum AircraftAgentType
    {
        FuelLoading,
        CargoLoading,
        PAXLoading
    }

    public enum BroadcastPopupEnum
    {
        Level = 0,
        SkillPoint = 1
    }

    public class BroadcastPopup
    {
        public BroadcastPopupEnum Type { get; set; }
        public string Text1 { get; set; }
        public string Text2 { get; set; }
        public string Text3 { get; set; }
        public Guid CompanyId { get; set; }
    }

    public class GetConnectedCompaniesResult
    {
        public bool[] CompanyIds { get; set; }
    }

    public class BroadcastEmployeesParams
    {
        public Guid? FlightId { get; set; }
        public Guid? PeopleId { get; set; }
    }

    public class BroadcastAircraftParams
    {
        public BroadcastUpdateAircraft Aircraft { get; set; }
        public Guid CompanyId { get; set; }
    }

    public class GetConnectedCompaniesParams
    {
        public Guid[] CompanyIds { get; set; }
    }

    public class AddWarpAgentParams
    {
        public Guid FlightId { get; set; }
        public DateTime WarpEndTime { get; set; }
    }

    public class PushParams
    {
        public Guid LogId { get; set; }
    }

    public class RefreshAircraftAfterSellParams
    {
        public Guid AircraftId { get; set; }
        public Guid CompanyId { get; set; }
    }

    public class AddPlayerAircraftParams
    {
        public Guid AircraftId { get; set; }
        public Guid FlightId { get; set; }
    }

    public class UpdatePlayerAircraftFromSimParams
    {
        public Guid AircraftId { get; set; }
        public Guid PeopleId { get; set; }
        public SimAircraftStatus SimAircraftStatus { get; set; }
        public AircraftStatusFlightTracker AircraftStatusFlightTracker { get; set; }
        public bool Completed { get; set; }
    }

    public class SimAircraftStatus
    {
        public string StatusDisplay;
        public short SimulationRate;
        public short AltimeterSetting;
        public short InternationalUnits;
        public bool PlaneOnGround;
        public bool StallWarning;
        public bool OverspeedWarning;
        public bool ApMasterSwitch;
        public int VerticalSpeed;
        public double VerticalSpeedAtTouchdown;
        public int Pitch;
        public int Bank;
        public int AltimeterAltitude;
        public double GForce;
        public double TotalVelocity;
        public double PressureAltitude;
        public double TrueAirspeed;
        public double Longitude;
        public double Latitude;
        public double Altitude;
        public double RadioAltitude;
        public double Compass;
        public double AirpeedKnots;
        public double DistanceFromDestination;
        public double BearingToDestination;
        public double GroundSpeed;
        public bool PlaneOnRunway;
        public DateTime FSTime;
        public DateTime FSZuluTime;
        public bool IsConnected;
        public bool HasCrashed;
        public bool IsPaused;
        public bool HasAborted;
        public string AircraftName;
        public string AircraftType;
        public string PayLoadDisplay;

        public double FuelTotalGallons;

        public double Eng1FF;
        public double Eng2FF;
        public double Eng3FF;
        public double Eng4FF;

        public double LoadedWeight;

        public int FuelTotalWeight;
        public byte FuelIsUnlimited;
        public short FuelWeight;
        public double FlapsLevel;
        public int WindDirection;
        public int WindSpeedKT;
        public int WindGustKT;

        public List<SimAircraftFuelTank> FuelTanks;

        public int ZeroFuelWeight { get; set; }
        public bool Slew { get; set; }

        public bool EnginesAreOff
        {
            get
            {
                return (Eng1FF < 1) &&
                   (Eng2FF < 1) &&
                   (Eng3FF < 1) &&
                   (Eng4FF < 1);
            }
        }

        public string AircraftAIRFile;
    }

    public enum AircraftStatusFlightTracker
    {
        WaitingForStart,
        EngineOn,
        DepartureTaxi,
        Airborne,
        Land,
        ArrivalTaxi,
        EngineOff
    }

    public class SimAircraftFuelTank
    {
        public double PercentFuel;
        public double Condition;
        public int SimOrder;
    }

    public class BroadcastUpdateAircraft
    {
        /*Propriétés nullable :
         * string : "-1"
         * Guid : new Guid() = 0000000
         * double : -1
         * int : -1
         * DateTime : new DateTime()
        */

        public BroadcastUpdateAircraft()
        {
            Date = DateTime.UtcNow;
        }

        public bool Delete { get; set; }

        public DateTime Date { get; set; }

        public Guid AircraftId { get; set; }

        public int? CurrentStatusDurationInMinutes { get; set; }
        public string NextWayPointICAO { get; set; }
        public double? NextWayPointLatitude { get; set; }
        public double? NextWayPointLongitude { get; set; }
        public string CurrentRunwayName { get; set; }
        public string FromTo { get; set; }
        public string Status { get; set; }
        public DateTime? EstimatedArrivalTime { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public double? Altitude { get; set; }
        public double? TotalFuel { get; set; }
        public double? Heading { get; set; }
        public double? Speed { get; set; }
        public double? VerticalSpeed { get; set; }
        public bool? IsControlledByAI { get; set; }
        public double? FOB { get; set; }
        public int? NextStatusUpdateSeconds { get; set; }

        // for Aircraft (DB)
        public DateTime? LastStatusChange { get; set; }
        public AircraftStatus? AircraftStatus { get; set; }
        public double? AirframeCondition { get; set; }
        public double? AirframeHours { get; set; }
        public Guid? CurrentAirportId { get; set; }
        public Guid? RentCompanyId { get; set; }
        public InFlightState? InFlightState { get; set; }
        public List<BroadcastUpdateAircraftEngine> Engines { get; set; }

        public bool? MustDoMaintenance { get; set; }
        public string MustDoMaintenanceDisplay { get; set; }

        public class BroadcastUpdateAircraftEngine
        {
            public int Number { get; set; }
            public double Condition { get; set; }
            public double Hours { get; set; }
        }
    }

    public class LoginParams
    {
        public Guid CompanyId { get; set; }
        public string Token { get; set; }
        public string CheckSum { get; set; }
    }

    public class LoginResult
    {
        public bool Logged { get; set; }
    }

    public class GetServerStatusParams
    {

    }

    public class GetServerStatusResult
    {
        public int TCPClients { get; set; }
        public int WorldObjects { get; set; }
        public string Message { get; set; }
        //public List<TCPClient> TCPClients { get; set; }
        //public List<WorldObject> WorldObjects { get; set; }

        //public class TCPClient
        //{
        //    public string IP { get; set; }
        //    public Dictionary<string, string> Properties { get; set; }
        //    public List<string> Stack { get; set; }
        //}

        //public class WorldObject
        //{
        //    public string DisplayName { get; set; }
        //    public DateTime LastTick { get; set; }
        //    public string State { get; set; }
        //}
    }

    public class GetAircraftAgentsParams
    {
        public Guid AircraftId { get; set; }
    }

    public class GetAircraftAgentsResult
    {
        public List<AircraftAgent> Agents { get; set; }

        public class AircraftAgent
        {
            public AircraftAgentType Type { get; set; }
            public Guid AircraftId { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public double Completion { get; set; }
            public double LoadDeltaValue { get; set; }
            public double UnloadDeltaValue { get; set; }
        }
    }

    public class AddAircraftAgentResult
    {
        public bool Success { get; set; }
        public string Error { get; set; }
    }

    public class AddAircraftAgentParams
    {
        public Guid AircraftId { get; set; }
        public AircraftAgentType Type { get; set; }
        public AircraftStatus AircraftEndStatus { get; set; }

        public Guid? PeopleId { get; set; }

        //Cargo + PAX
        public Guid? AirportId { get; set; }
        public double LoadedCargoLbs { get; set; }
        public int LoadedCharterPax { get; set; }
        public double UnloadedCargoLbs { get; set; }
        public int UnloadedCharterPax { get; set; }

        //Fuel
        public Guid? CompanyId { get; set; }
        public Guid? FBOId { get; set; }
        public FuelManifest CurrentFuelManifest { get; set; }
        public double DeltaFuelGal { get; set; }

        public class OnBoardManifest
        {
            public Guid FromAirportID { get; set; }
            public Guid ToAircraftID { get; set; }
            public Guid? CargoID { get; set; }
            public Guid? CharterID { get; set; }
            public double Weight { get; set; } //if cargo
            public int PassengersNumber { get; set; } // if charter
        }

        public class FuelManifest
        {
            public List<FuelTank> FuelTanks { get; set; }
            public class FuelTank
            {
                public Guid Id { get; set; }
                public double FuelPercent { get; set; }
            }
        }
    }

    public class RemoveAircraftParams
    {
        public Guid AircraftId { get; set; }
        public bool ForceKillWithoutComplete { get; set; }
        public bool IsPlayerAircraft { get; set; }
    }

    public class PauseAircraftParams
    {
        public Guid AircraftId { get; set; }
    }

    public class RemovePlayerAircraftParams
    {
        public Guid PeopleId { get; set; }
        public Guid AircraftId { get; set; }
    }

    public class SetPlayerAircraftValuesFromSimParams
    {
        public Guid PeopleId { get; set; }
        public Guid AircraftId { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double Altitude { get; set; }
        public double Heading { get; set; }
        public double Speed { get; set; }
        public double VerticalSpeed { get; set; }
        public double FOB { get; set; }
    }

    public class LaunchAircraftParams2
    {
        public Guid FlightId { get; set; }
    }

    public class LaunchAircraftParams
    {
        public Guid AircraftId { get; set; }
        public List<FlightPlanPoint> FlightPlan { get; set; }
        public double FlightLevel { get; set; }
        public Guid PilotID { get; set; }
        public Guid? CoPilotID { get; set; }
        public List<Guid> CrewIDs { get; set; }
        public List<Guid> PaxIDs { get; set; }

        public class FlightPlanPoint
        {
            public string ICAO { get; set; }
            public string Name { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public double Altitude { get; set; }
        }
    }

    public class GetCompanyAircraftsParams
    {
        public Guid CompanyId { get; set; }
        public int Index { get; set; }
        public int NumberOfAircrafts { get; set; }
    }

    public class GetCompanyAircraftsResult
    {
        public bool IsEnd { get; set; }
        public List<WorldAircraft> WorldAircrafts { get; set; }
        public class WorldAircraft
        {
            public Guid AircraftId { get; set; }

            public string NextWayPointICAO { get; set; }
            public double? NextWayPointLatitude { get; set; }
            public double? NextWayPointLongitude { get; set; }
            public string CurrentRunwayName { get; set; }
            public string FromTo { get; set; }
            public string Status { get; set; }
            public DateTime EstimatedArrivalTime { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public double Altitude { get; set; }
            public double Heading { get; set; }
            public double Speed { get; set; }
            public double VerticalSpeed { get; set; }
            public bool IsControlledByAI { get; set; }
            public double FOB { get; set; }
            public DateTime LastStatusChange { get; set; }
            public AircraftStatus AircraftStatus { get; set; }
            public double AirframeCondition { get; set; }
            public double AirframeHours { get; set; }
            public Guid? CurrentAirportId { get; set; }
            public InFlightState InFlightState { get; set; }
        }
    }
}
