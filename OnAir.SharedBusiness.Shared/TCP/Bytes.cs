﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnAir.SharedBusiness
{
    public static class Bytes
    {
        public static bool CompareBytesArray(byte[] buffer1, byte[] buffer2, int count)
        {
            var result = true;

            if (count == buffer1.Length)
            {
                for (var i = 0; i < count; i++)
                {
                    if (buffer2[i] != buffer1[i])
                    {
                        result = false;
                        break;
                    }
                }
            }
            else
            {
                result = false;
            }

            return result;
        }
    }
}
