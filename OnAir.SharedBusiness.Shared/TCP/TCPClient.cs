﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace OnAir.SharedBusiness
{
    public abstract class TCPClient
    {
        private const int MAX_STACK_SIZE = 5;
        private const int BUFFER_SIZE = 65536;
        public object _l = new object();

        public event EventHandler Disconnected;

        private const int BufferSize = 10240;
        public TcpClient TcpClient { get; set; }
        private byte[] _buffer;
        private bool _listenAsync;
        private byte[] _tcpReadBuffer = new byte[BUFFER_SIZE * MAX_STACK_SIZE];
        private int _tcpReadBufferIndex = 0;

        private Dictionary<Guid, TaskCompletionSource<byte[]>> _messages;

        public Action<string> Log { get; set; }

        public TCPClient()
        {
            _buffer = new byte[BufferSize];
            _messages = new Dictionary<Guid, TaskCompletionSource<byte[]>>();
        }

        protected abstract void OnMessageReceived(Guid messageId, byte[] bytes);

        public void Connect(string hostname, int port, bool listenAsync = true)
        {
            _listenAsync = listenAsync;

            TcpClient = new TcpClient()
            {
                SendTimeout = 60000,
                ReceiveTimeout = 60000
            };

            TcpClient.Connect(hostname, port);
            
            if (listenAsync)
            {
                ListenAsync();
            }
        }

        public async Task ConnectAsync(string hostname, int port)
        {
            TcpClient = new TcpClient()
            {
                SendTimeout = 60000,
                ReceiveTimeout = 60000
            };

            await TcpClient.ConnectAsync(hostname, port);
            
            ListenAsync();
        }

        public void Disconnect()
        {
            _continueListen = false;
            try
            {
                TcpClient?.Client?.Disconnect(false);
                TcpClient?.Client?.Close();
            }
            catch (Exception)
            {

            }
        }

        private bool _continueListen;

        private async Task ListenAsync()
        {
            _continueListen = true;

            while (_continueListen)
            {
                var buffer = new byte[BUFFER_SIZE];

                var bytesRead = -1;
                var bytesStacked = 0;
                var totalBytesRead = _tcpReadBufferIndex;

                try
                {
                    bytesRead = await TcpClient.GetStream().ReadAsync(buffer, 0, buffer.Length);
                }
                catch (Exception ex)
                {
                    _continueListen = false;
                    Disconnected?.Invoke(this, new EventArgs());
                    break;
                }
                Array.Copy(buffer, 0, _tcpReadBuffer, totalBytesRead, bytesRead);
                totalBytesRead += bytesRead;

                if (totalBytesRead == 0)
                {
                    return;
                }
                else
                {
                    var continueReading = true;

                    while (continueReading)
                    {
                        var nextMessageLength = -1;
                        var enoughBytesToRead = true;

                        if (totalBytesRead - bytesStacked < 18) //Si _tcpReadBuffer n'a pas encore 18 bytes, on a pas le message length ni l'id, on attend la suite
                        {
                            enoughBytesToRead = false;
                        }
                        else
                        {
                            //On vérifie qu'on a au moins "messageLength" bytes dans _tcpReadBuffer pour stacker
                            var nextMessageLengthBytes = new byte[2];
                            Array.Copy(_tcpReadBuffer, bytesStacked, nextMessageLengthBytes, 0, 2);
                            nextMessageLength = BitConverter.ToInt16(nextMessageLengthBytes, 0);
                            if (nextMessageLength < 18)
                            {
                                throw new IllegalAccessDetectedException("Message Length must be >= 18");
                            }
                            enoughBytesToRead = (bytesStacked + nextMessageLength) <= totalBytesRead;
                        }

                        if (enoughBytesToRead)
                        {
                            //HERE, unstack the message

                            //Lecture de l'ID
                            var messageIdBytes = new byte[16];
                            Array.Copy(_tcpReadBuffer, bytesStacked + 2, messageIdBytes, 0, 16);
                            var messageId = new Guid(messageIdBytes);

                            //Lecture du contenu du message (bytes)
                            var bytes = new byte[nextMessageLength - 16 - 2];
                            Array.Copy(_tcpReadBuffer, bytesStacked + 16 + 2, bytes, 0, bytes.Length);
                            bytesStacked += nextMessageLength;

                            if (_messages.ContainsKey(messageId))
                            {
                                var tcs = _messages[messageId];

                                if (Bytes.CompareBytesArray(TCPParams.NACK, bytes, 4))
                                {
                                    var error = Encoding.UTF8.GetString(bytes, 4, bytes.Length - 4);
                                    tcs.SetException(new Exception(error));
                                }
                                else
                                {
                                    tcs.SetResult(bytes);
                                }

                                OnMessageReceived(messageId, bytes);
                            }
                            else
                            {
                                OnMessageReceived(messageId, bytes);
                            }

                            if (bytesStacked == totalBytesRead)
                            {
                                continueReading = false;
                                _tcpReadBufferIndex = 0;
                            }
                        }
                        else
                        {
                            //All messages unstacked, _tcpReadBuffer peut contenir le début du prochain message
                            continueReading = false;

                            var remainingBytesToRead = totalBytesRead - bytesStacked;
                            _tcpReadBufferIndex = remainingBytesToRead;

                            if (bytesStacked > 0)
                            {
                                Array.Copy(_tcpReadBuffer, bytesStacked, _tcpReadBuffer, 0, remainingBytesToRead);
                            }
                        }
                    }
                }
            }
        }

        public Task<byte[]> SendAsync(byte[] bytes)
        {
            var messageId = Guid.NewGuid();
            var tcs = new TaskCompletionSource<byte[]>();

            _messages.Add(messageId, tcs);

            Write(messageId, bytes);

            return tcs.Task;
        }

        public async Task<byte[]> SendCommandAsync(CommandType type, byte[] bytes)
        {
            var commandBytes = new byte[bytes.Length + 1];

            commandBytes[0] = Convert.ToByte((int)type);
            Array.Copy(bytes, 0, commandBytes, 1, bytes.Length);

            return await SendAsync(commandBytes);
        }

        public byte[] SendCommand(CommandType type, byte[] bytes)
        {
            var commandBytes = new byte[bytes.Length + 1];

            commandBytes[0] = Convert.ToByte((int)type);
            Array.Copy(bytes, 0, commandBytes, 1, bytes.Length);

            return SendAndWaitForResponse(commandBytes);
        }

        private byte[] SendAndWaitForResponse(byte[] bytes)
        {
            if (_listenAsync == true)
            {
                throw new Exception("Error SendAndWaitForResponse _listenAsync is true");
            }

            var messageId = Guid.NewGuid();
            Write(messageId, bytes);

            var bytesRead = TcpClient.GetStream().Read(_buffer, 0, _buffer.Length);

            if (bytesRead >= 18)
            {
                var content = new byte[bytesRead - 18];
                Array.Copy(_buffer, 18, content, 0, content.Length);
                return content;
            }

            return null;
        }

        private void Write(Guid messageId, byte[] bytes)
        {
            var messageIdBytes = messageId.ToByteArray();

            var messageLength = messageIdBytes.Length + bytes.Length + 2;
            if (messageLength > Math.Pow(2, 16))
            {
                throw new Exception("Message is too long");
            }
            var messageLengthInt16 = Convert.ToInt16(messageLength);
            var messageLengthBytes = BitConverter.GetBytes(messageLengthInt16);

            var message = new byte[bytes.Length + messageIdBytes.Length + 2]; //2 = length du message

            Array.Copy(messageLengthBytes, message, 2);
            Array.Copy(messageIdBytes, 0, message, 2, messageIdBytes.Length);
            Array.Copy(bytes, 0, message, 2 + messageIdBytes.Length, bytes.Length);

            TcpClient.GetStream().Write(message, 0, message.Length);
        }

        public static string ComputeCheckSum(Guid companyId)
        {
            var result = string.Empty;

            var bytes = companyId.ToByteArray();

            var byte3 = bytes[3];
            var byte4 = bytes[4];
            var byte5 = bytes[5];

            var byte7 = bytes[7];
            var byte12 = bytes[12];

            int t = byte3 + byte4 + byte5 + byte7 + byte12;

            return t.ToString();
        }
    }
}
