﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnAir.SharedBusiness
{
    public class IllegalAccessDetectedException : Exception
    {
        public IllegalAccessDetectedException()
        {

        }

        public IllegalAccessDetectedException(string message) : base(message)
        {

        }
    }
}
