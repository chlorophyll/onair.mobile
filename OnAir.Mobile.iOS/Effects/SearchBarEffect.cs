﻿using System;
using Ecommunity.Mobile;
using OnAir.Mobile.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportEffect(typeof(SearchBarEffect), "SearchBarEffect")]
namespace OnAir.Mobile.iOS
{
    public class SearchBarEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            var searchBar = Control as UISearchBar;
            searchBar.BackgroundColor = UIColor.Clear;
            searchBar.BarTintColor = UIColor.Clear;
            searchBar.BackgroundImage = new UIImage();
            searchBar.Translucent = true;
            searchBar.SearchBarStyle = UISearchBarStyle.Minimal;
        }

        protected override void OnDetached()
        {
        }
    }
}

public class SearchBarCustomizer : IControlCustomizer
{
    public void CustomizeControlBeforeRendering(Ecommunity.Mobile.Components.Control control)
    {

    }

    public void CustomizeControlBeforeContextChanged(Ecommunity.Mobile.Control control)
    {

    }
    public void CustomizeControlAfterContextChanged(Ecommunity.Mobile.Control control)
    {

    }

    public void CustomizeControlAfterRendering(Ecommunity.Mobile.Control control)
    {
        var view = control.GetView();
        view.Effects.Add(new SearchBarEffect());
    }
}
