﻿using System;
using Ecommunity.Mobile;
using Ecommunity.Mobile.Components;
using OnAir.Mobile.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportEffect(typeof(EntryRemoveBorderLineEffect), "EntryRemoveBorderLineEffect")]
namespace OnAir.Mobile.iOS
{
    public class EntryRemoveBorderLineEffect : PlatformEffect
    {

        protected override void OnAttached()
        {
            var editText = Control as UITextField;
            editText.BorderStyle = UITextBorderStyle.None;
        }

        protected override void OnDetached()
        {

        }
    }

    public class EntryCustomizer : IControlCustomizer
    {
        public void CustomizeControlBeforeRendering(Ecommunity.Mobile.Components.Control control)
        {

        }

        public void CustomizeControlBeforeContextChanged(Ecommunity.Mobile.Control control)
        {

        }
        public void CustomizeControlAfterContextChanged(Ecommunity.Mobile.Control control)
        {

        }

        public void CustomizeControlAfterRendering(Ecommunity.Mobile.Control control)
        {
            var view = control.GetView();
            view.Effects.Add(new EntryRemoveBorderLineEffect());
        }
    }

    public class V2ComboxBoxCustomizer : IControlCustomizer
    {
        public void CustomizeControlBeforeRendering(Ecommunity.Mobile.Components.Control control)
        {

        }

        public void CustomizeControlBeforeContextChanged(Ecommunity.Mobile.Control control)
        {

        }
        public void CustomizeControlAfterContextChanged(Ecommunity.Mobile.Control control)
        {

        }

        public void CustomizeControlAfterRendering(Ecommunity.Mobile.Control control)
        {
            var view = control.GetView();
            view.Effects.Add(new EntryRemoveBorderLineEffect());
        }
    }

    public class V2DatePickerCustomizer : IControlCustomizer
    {
        public void CustomizeControlBeforeRendering(Ecommunity.Mobile.Components.Control control)
        {

        }

        public void CustomizeControlBeforeContextChanged(Ecommunity.Mobile.Control control)
        {

        }
        public void CustomizeControlAfterContextChanged(Ecommunity.Mobile.Control control)
        {

        }

        public void CustomizeControlAfterRendering(Ecommunity.Mobile.Control control)
        {
            var view = control.GetView();
            view.Effects.Add(new EntryRemoveBorderLineEffect());
        }
    }
}