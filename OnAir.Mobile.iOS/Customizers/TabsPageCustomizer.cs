﻿using System;
using Ecommunity.Mobile;
using Xamarin.Forms;

namespace OnAir.Mobile.iOS
{
    public class MissionTabDetailPageCustomizer : IPageCustomizer
    {
        public void CustomizePage(Ecommunity.Mobile.Components.Page componentsPage, Ecommunity.Mobile.Page ecommunityPage, Xamarin.Forms.Page xamarinPage)
        {
            xamarinPage.IconImageSource = ImageSource.FromFile("file.png");
        }
    }

    public class MissionTabMapPageCustomizer : IPageCustomizer
    {
        public void CustomizePage(Ecommunity.Mobile.Components.Page componentsPage, Ecommunity.Mobile.Page ecommunityPage, Xamarin.Forms.Page xamarinPage)
        {
            xamarinPage.IconImageSource = ImageSource.FromFile("map.png");
        }
    }
}
