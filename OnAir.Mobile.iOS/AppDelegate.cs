﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;

namespace OnAir.Mobile.iOS
{

    [Register("AppDelegate")]
    public partial class AppDelegate : Ecommunity.Mobile.iOS.GenericAppDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Ecommunity.Mobile.iOS.iOSServices.RegisterAllServices();
            global::Xamarin.Forms.Forms.Init();
            Xamarin.FormsMaps.Init();
            ZXing.Net.Mobile.Forms.iOS.Platform.Init();

            LoadApplication(new App());

            Ecommunity.Mobile.EcommunityMobile.RegisterControlCustomizer(typeof(Ecommunity.Mobile.Entry), new EntryCustomizer());
            Ecommunity.Mobile.EcommunityMobile.RegisterControlCustomizer(typeof(Ecommunity.Mobile.V2ComboBox), new V2ComboxBoxCustomizer());
            Ecommunity.Mobile.EcommunityMobile.RegisterControlCustomizer(typeof(Ecommunity.Mobile.V2DatePicker), new V2DatePickerCustomizer());
            Ecommunity.Mobile.EcommunityMobile.RegisterControlCustomizer(typeof(Ecommunity.Mobile.V2SearchBar), new SearchBarCustomizer());
            Ecommunity.Mobile.EcommunityMobile.RegisterPageCustomizer(Ids.phone320568.MissionTabDetail.Id, new MissionTabDetailPageCustomizer());
            Ecommunity.Mobile.EcommunityMobile.RegisterPageCustomizer(Ids.phone320568.MissionTabMAP.Id, new MissionTabMapPageCustomizer());

            return base.FinishedLaunching(app, options);
        }
    }
}