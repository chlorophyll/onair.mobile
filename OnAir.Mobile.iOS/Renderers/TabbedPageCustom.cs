﻿using System;
using Ecommunity.Mobile;
using OnAir.Mobile.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomTabbedPage), typeof(TabbedPageCustom))]
namespace OnAir.Mobile.iOS
{
    public class TabbedPageCustom : TabbedRenderer
    {
        public override void ViewWillLayoutSubviews()
        {
            base.ViewWillLayoutSubviews();

            int SystemVersion = Convert.ToInt16(UIDevice.CurrentDevice.SystemVersion.Split('.')[0]);

            if (SystemVersion >= 10)
            {
                TabBar.SelectedImageTintColor = UIColor.White;
                TabBar.TintColor = UIColor.White;
                TabBar.UnselectedItemTintColor = UIColor.DarkGray;
            }
        }
    }
}
